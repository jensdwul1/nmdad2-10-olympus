<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'Core'], function () {
    Route::get('/', 'HomeController@index')->name('home');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin'], function () {

    Route::get('/', 'DashboardController@index')->name('admin');
    Route::get('/referendums/actives', 'ReferendumsController@actives');
    Route::get('/referendums/check', 'ReferendumsController@check');
    Route::resource('referendums', 'ReferendumsController');
    Route::post('/referendums/{referendum}/active','ReferendumsController@active');
    Route::delete('/referendums/delete/{referendum}','ReferendumsController@destroy');
    Route::delete('/referendums/crush/{referendum}','ReferendumsController@crush');

    Route::resource('gods', 'GodsController');
    Route::delete('/gods/delete/{god}','GodsController@destroy');
    Route::delete('/gods/crush/{god}','GodsController@crush');

    Route::resource('elections', 'ElectionsController');
    Route::delete('/elections/delete/{election}','ElectionsController@destroy');
    Route::delete('/elections/crush/{election}','ElectionsController@crush');

    Route::resource('candidates', 'CandidatesController');
    Route::delete('/candidates/delete/{candidate}','CandidatesController@destroy');
    Route::delete('/candidates/crush/{candidate}','CandidatesController@crush');

    Route::resource('pantheons', 'PantheonsController');
    Route::delete('/pantheons/delete/{pantheon}','PantheonsController@destroy');
    Route::delete('/pantheons/crush/{pantheon}','PantheonsController@crush');

    Route::resource('members', 'PantheonMembersController');
    Route::delete('/members/delete/{member}','PantheonMembersController@destroy');
    Route::delete('/members/crush/{member}','PantheonMembersController@crush');

    Route::resource('comments', 'CommentsController');
    Route::delete('/comments/delete/{comment}','CommentsController@destroy');
    Route::delete('/comments/crush/{comment}','CommentsController@crush');

    Route::resource('achievements', 'AchievementsController');
    Route::delete('/achievements/delete/{achievement}','AchievementsController@destroy');
    Route::delete('/achievements/crush/{achievement}','AchievementsController@crush');

    Route::resource('categories', 'CategoriesController');
    Route::delete('/categories/delete/{category}','CategoriesController@destroy');
    Route::delete('/categories/crush/{category}','CategoriesController@crush');

    Route::resource('users', 'UsersController');
    Route::delete('/users/delete/{user}','UsersController@destroy');
    Route::delete('/users/crush/{user}','UsersController@crush');

    Route::resource('admins', 'AdminsController');
    Route::delete('/admins/delete/{user}','AdminsController@destroy');
    Route::delete('/admins/crush/{user}','AdminsController@crush');
});

Auth::routes();

