<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    'namespace' => 'API',
    'middleware' => 'cors'
    ],
    function(){
        $options = [
            'except' => [
                'create',
                'edit'
            ]
        ];
        Route::resource('candidates','CandidatesController',$options);
        Route::post('login','LoginController@login');
        Route::post('register','LoginController@register');
        Route::post('update','LoginController@update');
        Route::get('electionsofmonth','ElectionsController@monthly');
        Route::resource('elections','ElectionsController',$options);
        Route::resource('votes','VotesController',$options);
        Route::get('uservotes/{user}','VotesController@showByUser');
        Route::resource('gods','GodsController',$options);
        Route::resource('comments','CommentsController',$options);
        Route::resource('categories','CategoriesController',$options);
        Route::resource('pantheons','PantheonsController',$options);
        Route::get('referendums/{referendum}/comments','ReferendumsController@fetchComments');
        Route::resource('referendums','ReferendumsController',$options);
        Route::resource('users','UsersController',$options);
});