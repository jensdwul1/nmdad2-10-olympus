<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Pantheon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PantheonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Pantheon::apiFetchAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'startDate' => 'required',
            'endDate' => 'required',
        ];

        $this->validate($request, $rules);

        $pantheon = Pantheon::create([
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
        ]);

        if ($pantheon->save()) {
            return response()
                ->json($pantheon)
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pantheon = new Pantheon();
        $pantheon = $pantheon->apiFetch($id);
        return $pantheon ?: response()
            ->json([
                'error' => "Pantheon `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pantheon = Pantheon::withTrashed()
            ->where('id', $id)
            ->first();

        $rules = [
            'startDate' => 'required',
            'endDate' => 'required',
        ];

        $validator = Validator::make($request->input(), $rules);
        $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $pantheon->startDate = request()->startDate;
        $pantheon->endDate = request()->endDate;

        if ($pantheon->save()) {
            return response()
                ->json($pantheon)
                ->setStatusCode(Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pantheon = Pantheon::find($id);

        if ($pantheon) {
            if ($pantheon->delete()) {
                return response()
                    ->json($pantheon)
                    ->setStatusCode(Response::HTTP_OK);
            }

            return response()
                ->json([
                    'error' => "Pantheon `${id}` could not be deleted",
                ])
                ->setStatusCode(Response::HTTP_CONFLICT);
        }

        return response()
            ->json([
                'error' => "Pantheon `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}
