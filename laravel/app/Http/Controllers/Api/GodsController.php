<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\God;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GodsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return God::with('elections')->orderBy('title')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'icon' => 'required',
        ];

        $this->validate($request, $rules);

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            if ($request->file('icon')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('icon');
                $destinationPath = 'img/gods';
                $fileName = str_replace(" ", "-",$request->title).'.'.$file->guessExtension();
                $file->move($destinationPath,$fileName);
                $request->merge(['icon' => $fileName]);
            }
        }

        $god = God::create([
            'title' => $request->title,
            'body' => $request->body,
            'icon' => $fileName,
        ]);
        if ($god->save()) {
            return response()
                ->json($god)
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $god = new God();
        $god = $god->apiFetch($id);

        return $god ?: response()
            ->json([
                'error' => "God `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $god = God::withTrashed()
            ->where('id', $id)
            ->first();

        $rules = [
            'title' => 'required',
        ];

        $validator = Validator::make($request->input(), $rules);
        $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        $god->title = $request->title;
        if($request->body){
            $god->body = $request->body;
        }

        if(count($request->categories) > 0){
            $god->categories()->sync($request->categories);
        } else {
            $god->categories()->sync([]);
        }

        if ($god->save()) {
            return response()
                ->json($god)
                ->setStatusCode(Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $god = God::find($id);

        if ($god) {
            if ($god->delete()) {
                return response()
                    ->json($god)
                    ->setStatusCode(Response::HTTP_OK);
            }

            return response()
                ->json([
                    'error' => "God `${id}` could not be deleted",
                ])
                ->setStatusCode(Response::HTTP_CONFLICT);
        }

        return response()
            ->json([
                'error' => "God `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}
