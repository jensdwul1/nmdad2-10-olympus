<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Category::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'god' => 'required'
        ];

        $this->validate($request, $rules);


        $category = Category::create([
            'title' => $request->title,
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
            'god_id' => $request->god,
        ]);

        if ($category->save()) {
            return response()
                ->json($category)
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = new Category();
        $category = $category->apiFetch($id);
        return $category ?: response()
            ->json([
                'error' => "Category `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::withTrashed()
            ->where('id', $id)
            ->first();

        $rules = [
            'title' => $request->title,
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
            'god_id' => $request->god,
        ];

        $validator = Validator::make($request->input(), $rules);
        $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $category->title = request()->title;
        $category->startDate = request()->startDate;
        $category->endDate = request()->endDate;
        $category->god_id = request()->god;

        if ($category->save()) {
            return response()
                ->json($category)
                ->setStatusCode(Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if ($category) {
            if ($category->delete()) {
                return response()
                    ->json($category)
                    ->setStatusCode(Response::HTTP_OK);
            }

            return response()
                ->json([
                    'error' => "Category `${id}` could not be deleted",
                ])
                ->setStatusCode(Response::HTTP_CONFLICT);
        }

        return response()
            ->json([
                'error' => "Category `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}
