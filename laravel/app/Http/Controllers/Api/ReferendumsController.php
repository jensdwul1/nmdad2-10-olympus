<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Referendum;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class ReferendumsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Referendum::apiFetchAll();
    }

    public function fetchComments($id){

        $comment = new Comment();
        $comments = $comment->apiFetchByReferendum($id);
        return $comments ?: response()
            ->json([
                'error' => "Comments from user `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'categories' => 'required',
            'user' => 'required'
        ];

        $this->validate($request, $rules);
        $startDate = date('Y-m-d', strtotime('first day of next month'));
        $endDate = date('Y-m-d', strtotime('last day next month'));

        $referendum = Referendum::create([
            'title' => $request->title,
            'description' => $request->body,
            'startDate' => $startDate,
            'endDate' => $endDate,
            'user_id' => $request->user,
        ]);
        $referendum->categories()->sync($request->categories);

        if ($referendum->save()) {
            return response()
                ->json($referendum)
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $referendum = new Referendum();
        $referendum = $referendum->apiFetch($id);
        return $referendum ?: response()
            ->json([
                'error' => "Referendum `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $referendum = Referendum::withTrashed()
            ->where('id', $id)
            ->first();

        $rules = [
            'categories' => 'required',
        ];

        $validator = Validator::make($request->input(), $rules);
        $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        $referendum->description = $request->body;
        $referendum->categories()->sync($request->categories);

        if ($referendum->save()) {
            return response()
                ->json($referendum)
                ->setStatusCode(Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $referendum = Referendum::find($id);

        if ($referendum) {
            if ($referendum->delete()) {
                return response()
                    ->json($referendum)
                    ->setStatusCode(Response::HTTP_OK);
            }

            return response()
                ->json([
                    'error' => "Referendum `${id}` could not be deleted",
                ])
                ->setStatusCode(Response::HTTP_CONFLICT);
        }

        return response()
            ->json([
                'error' => "Referendum `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}
