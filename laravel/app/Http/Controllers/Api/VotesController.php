<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Vote::apiFetchAll();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'user' => 'required',
            'value' => 'required',
        ];

        $this->validate($request, $rules);


        $vote = Vote::create([
            'value' => $request->value,
            'user_id' => $request->user,
            'referendum_id' => ($request->referendum)?$request->referendum:null,
            'election_id' => ($request->election)?$request->election:null,
        ]);

        if ($vote->save()) {
            return response()
                ->json($vote)
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vote = new Vote();
        $vote = $vote->apiFetch($id);
        return $vote ?: response()
            ->json([
                'error' => "Vote `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }


    public function showByUser($id){

        $vote = new Vote();
        $votes = $vote->apiFetchByUser($id);
        return $votes ?: response()
            ->json([
                'error' => "Votes from user `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vote = Vote::withTrashed()
            ->where('id', $id)
            ->first();

        $rules = [
            'value' => $request->value,
            'user_id' => $request->user_id,
        ];

        $validator = Validator::make($request->input(), $rules);
        $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $vote->value = request()->title;
        $vote->user_id = request()->user;
        $vote->election_id = request()->election;
        $vote->referendum_id = request()->referendum;

        if ($vote->save()) {
            return response()
                ->json($vote)
                ->setStatusCode(Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vote = Vote::find($id);

        if ($vote) {
            if ($vote->delete()) {
                return response()
                    ->json($vote)
                    ->setStatusCode(Response::HTTP_OK);
            }

            return response()
                ->json([
                    'error' => "Vote `${id}` could not be deleted",
                ])
                ->setStatusCode(Response::HTTP_CONFLICT);
        }

        return response()
            ->json([
                'error' => "Vote `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}
