<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'firstName' => 'required',
            'sex' => 'required',
            'picture' => 'required',
        ];

        $this->validate($request, $rules);

        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            if ($request->file('picture')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('picture');
                $destinationPath = 'img/users';

                $fileName = $request->id.'.'.$file->guessExtension();
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->profile_picture = $fileName;
            }
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => str_random(10),
        ]);

        $profile = [
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'sex' => $request->sex,
            'dateOfBirth' => $request->dateOfBirth,
            'phone' => $request->phone,
            'country' => $request->country,
            'job' => $request->job,
            'description' => $request->description,
            'profile_picture' => $request->profile_picture,
        ];
        $user->profile()->save($profile);

        if ($user->save()) {
            return response()
                ->json($user)
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = new User();
        $user = $user->apiFetch($id);
        
        return $user ?: response()
            ->json([
                'error' => "User `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::withTrashed()
            ->where('id', $id)
            ->first();

        $rules = [
            'firstName' => 'required',
            'sex' => 'required',
        ];

        $validator = Validator::make($request->input(), $rules);
        $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }


        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            if ($request->file('picture')->isValid()) {
                //$request->file('picture')->store('img');
                $file = $request->file('picture');
                $destinationPath = 'img/users';

                $fileName = $user->id.'_'.$request->firstName.'.'.$file->guessExtension();
                //Delete Previous Image
                File::delete($destinationPath,$user->profile->profile_picture);
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->merge(['picture' => $fileName]);
                $user->profile->profile_picture = $fileName;
            }
        }

        $user->profile->firstName = $request->firstName;
        $user->profile->lastName = $request->lastName;
        $user->profile->sex = $request->sex;
        $user->profile->dateOfBirth = $request->dateOfBirth;
        $user->profile->phone = $request->phone;
        $user->profile->country = $request->country;
        $user->profile->job = $request->job;
        $user->profile->description = $request->description;
        $user->profile->save();

        if ($user->save()) {
            return response()
                ->json($user)
                ->setStatusCode(Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user) {
            if ($user->delete()) {
                return response()
                    ->json($user)
                    ->setStatusCode(Response::HTTP_OK);
            }

            return response()
                ->json([
                    'error' => "User `${id}` could not be deleted",
                ])
                ->setStatusCode(Response::HTTP_CONFLICT);
        }

        return response()
            ->json([
                'error' => "User `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}
