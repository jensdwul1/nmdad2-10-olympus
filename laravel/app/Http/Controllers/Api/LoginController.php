<?php
/**
 * Created by PhpStorm.
 * User: jdwje
 * Date: 12-5-2017
 * Time: 09:03
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class LoginController extends Controller
{
    public function login(Request $request){
        $email = request('email');
        $password = request('password');
        $passwordHashed = User::where('email', $email)
            ->value('password');
        if (\Hash::check($password, $passwordHashed)) {
            $user = User::where('email', $email)
                ->get();
            return $user ?: response()
                ->json([
                    'error' => "User `${$email}` not found",
                ])
                ->setStatusCode(Response::HTTP_NOT_FOUND);
        } else {
            return "password incorrect";
        }
    }
    public function register(Request $request){
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
            'firstName' => 'required',
            'sex' => 'required',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => str_random(10),
        ]);

        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            if ($request->file('picture')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('picture');
                $destinationPath = 'img/users';

                $fileName = $user->id.'_'.$request->firstName.'.'.$file->guessExtension();
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->profile_picture = $fileName;
            }
        } else {
            $request->profile_picture = 'lego/'.rand(0,8).'.jpg';
        }
        $profile = new Profile([
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'sex' => $request->sex,
            'dateOfBirth' => $request->dateOfBirth,
            'phone' => $request->phone,
            'country' => $request->country,
            'job' => $request->job,
            'description' => $request->description,
            'profile_picture' => $request->profile_picture,
        ]);
        $user->profile()->save($profile);

        return $user ?: response()
            ->json([
                'error' => "User creation error",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    public function update(Request $request){

    }
}