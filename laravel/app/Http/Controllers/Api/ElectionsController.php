<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Election;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ElectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Election::apiFetchAll();
    }

    public function monthly()
    {
        $startDate = date('Y-m-d', strtotime('first day of next month'));
        return Election::where('startDate',$startDate)->get();
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'god' => 'required'
        ];

        $this->validate($request, $rules);


        $election = Election::create([
            'title' => $request->title,
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
            'god_id' => $request->god,
        ]);

        if ($election->save()) {
            return response()
                ->json($election)
                ->setStatusCode(Response::HTTP_CREATED);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $election = new Election();
        $election = $election->apiFetch($id);
        return $election ?: response()
            ->json([
                'error' => "Election `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $election = Election::withTrashed()
            ->where('id', $id)
            ->first();

        $rules = [
            'title' => $request->title,
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
            'god_id' => $request->god,
        ];

        $validator = Validator::make($request->input(), $rules);
        $this->validate($request, $rules);

        if ($validator->fails()) {
            return response()
                ->json([
                    'errors' => $validator->errors()->all(),
                ])
                ->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $election->title = request()->title;
        $election->startDate = request()->startDate;
        $election->endDate = request()->endDate;
        $election->god_id = request()->god;

        if ($election->save()) {
            return response()
                ->json($election)
                ->setStatusCode(Response::HTTP_OK);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $election = Election::find($id);

        if ($election) {
            if ($election->delete()) {
                return response()
                    ->json($election)
                    ->setStatusCode(Response::HTTP_OK);
            }

            return response()
                ->json([
                    'error' => "Election `${id}` could not be deleted",
                ])
                ->setStatusCode(Response::HTTP_CONFLICT);
        }

        return response()
            ->json([
                'error' => "Election `${id}` not found",
            ])
            ->setStatusCode(Response::HTTP_NOT_FOUND);
    }
}
