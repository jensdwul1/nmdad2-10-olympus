<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\Referendum;
use App\Models\Election;
use App\Models\Vote;
use App\Models\God;
use App\Models\User;
use App\Models\Pantheon;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $month = date('Y-m-t');

        $cur_pantheon = Pantheon::where('endDate', $month)->first();
        $cur_referendums = count(Referendum::where('active','1')->get());
        $lat_referendum = Referendum::latest()->first();
        $users = count(User::whereDate('created_at', DB::raw('CURDATE()'))->get());
        //dd($cur_pantheon,$cur_referendums,$lat_referendum,$users);
        return view('backoffice.dashboard.index',compact('cur_pantheon','cur_referendums','lat_referendum','users'));
    }
}
