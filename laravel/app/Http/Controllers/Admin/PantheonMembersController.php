<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pantheon;
use App\Models\PantheonMember;
use App\Models\God;
use App\Models\Profile;

class PantheonMembersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $members = PantheonMember::withTrashed()->orderBy('user_id','asc')->paginate(12);
        return view('backoffice.members.index',compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pantheons = Pantheon::orderBy('startDate','asc')->get();
        $gods = God::all();
        $profiles = Profile::orderBy('firstName','asc')->get();
        return view('backoffice.members.create',compact('pantheons','gods','profiles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request = request();
        $this->validate($request,[
            'user' => 'required',
            'god' => 'required',
            'pantheon' => 'required'
        ]);

        PantheonMember::create([
            'user_id' => $request->user,
            'pantheon_id' => $request->pantheon,
            'god_id' => $request->god,
        ]);
        return redirect('/admin/members');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = PantheonMember::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.members.show',compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = PantheonMember::withTrashed()
            ->where('id', $id)
            ->first();
        $gods = God::all();
        $pantheons = Pantheon::all();
        $profiles = Profile::orderBy('firstName','asc')->get();
        return view('backoffice.members.edit',compact('member','pantheons','profiles','gods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $member = PantheonMember::withTrashed()
            ->where('id', $id)
            ->first();

        $request = request();
        $this->validate($request,[
            'user' => 'required',
            'god' => 'required',
            'pantheon' => 'required'
        ]);

        $member->user_id = request()->user;
        $member->pantheon_id = request()->pantheon;
        $member->god_id = request()->god;

        $member->save();
        return redirect('/admin/members');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $member = PantheonMember::withTrashed()
            ->where('id', $id)
            ->first();
        if($member->trashed()){
            $member->restore();
        } else {
            $member->delete();
        }
        return redirect('/admin/members');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $member = PantheonMember::withTrashed()
            ->where('id', $id)
            ->first();

        $member->forceDelete();
        return redirect('/admin/members');
    }
}
