<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Candidate;
use App\Models\Election;
use App\Models\Profile;
use Illuminate\Http\Request;

class CandidatesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $candidates = Candidate::withTrashed()->orderBy('id','asc')->paginate(12);
        return view('backoffice.candidates.index',compact('candidates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $elections = Election::all();
        $profiles = Profile::orderBy('firstName','asc')->get();
        return view('backoffice.candidates.create',compact('elections','profiles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'user' => 'required',
            'election' => 'required'
        ]);

        Candidate::create([
            'user_id' => $request->title,
            'election_id' => $request->election,
        ]);
        return redirect('/admin/candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidate = Candidate::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.candidates.show',compact('candidate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::withTrashed()
            ->where('id', $id)
            ->first();

        $elections = Election::all();
        $profiles = Profile::orderBy('firstName','asc')->get();
        return view('backoffice.candidates.edit',compact('candidate','elections','profiles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $candidate = Candidate::withTrashed()
            ->where('id', $id)
            ->first();

        $request = request();
        $this->validate($request,[
            'user' => 'required',
            'election' => 'required'
        ]);

        $candidate->user_id = request()->user;
        $candidate->election_id = request()->election;

        $candidate->save();
        return redirect('/admin/candidates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate = Candidate::withTrashed()
            ->where('id', $id)
            ->first();
        if($candidate->trashed()){
            $candidate->restore();
        } else {
            $candidate->delete();
        }
        return redirect('/admin/candidates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $candidate = Candidate::withTrashed()
            ->where('id', $id)
            ->first();

        $candidate->forceDelete();
        return redirect('/admin/candidates');
    }
}
