<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Election;
use App\Models\God;
use Illuminate\Http\Request;

class ElectionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $elections = Election::withTrashed()->orderBy('startDate','asc')->paginate(12);
        return view('backoffice.elections.index',compact('elections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gods = God::all();
        return view('backoffice.elections.create',compact('gods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'god' => 'required'
        ]);

        Election::create([
            'title' => $request->title,
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
            'god_id' => $request->god,
        ]);
        return redirect('/admin/elections');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $election = Election::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.elections.show',compact('election'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $election = Election::withTrashed()
            ->where('id', $id)
            ->first();

        $gods = God::all();
        return view('backoffice.elections.edit',compact('election','gods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $election = Election::withTrashed()
            ->where('id', $id)
            ->first();

        $request = request();
        $this->validate($request,[
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'god' => 'required'
        ]);

        $election->title = request()->title;
        $election->startDate = request()->startDate;
        $election->endDate = request()->endDate;
        $election->god_id = request()->god;

        $election->save();
        return redirect('/admin/elections');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $election = Election::withTrashed()
            ->where('id', $id)
            ->first();
        if($election->trashed()){
            $election->restore();
        } else {
            $election->delete();
        }
        return redirect('/admin/elections');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $election = Election::withTrashed()
            ->where('id', $id)
            ->first();

        $election->forceDelete();
        return redirect('/admin/elections');
    }
}
