<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\God;
use File;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $categories = Category::withTrashed()->orderBy('title','asc')->paginate(12);
        return view('backoffice.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'title' => 'required'
        ]);

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            if ($request->file('icon')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('icon');
                $destinationPath = 'img/categories';
                $fileName = str_replace(" ", "-",$request->title).'.'.$file->guessExtension();
                $file->move($destinationPath,$fileName);
                $request->merge(['icon' => $fileName]);
            }
        } else {
            $fileName = NULL;
        }

        Category::create([
            'title' => $request->title,
            'description' => $request->description,
            'icon' => ($fileName)? $fileName : null,
        ]);
        return redirect('/admin/categories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.categories.show',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::withTrashed()
            ->where('id', $id)
            ->first();
        $gods = God::orderBy('title','asc')->get();
        $godsArray = array_column($category->gods->toArray(), 'id');
        return view('backoffice.categories.edit',compact('category','gods','godsArray'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $category = Category::withTrashed()
            ->where('id', $id)
            ->first();
        $request = request();

        $this->validate($request,[
            'title' => 'required'
        ]);

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            if ($request->file('icon')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('icon');
                $destinationPath = 'img/categories';

                $fileName = str_replace(" ", "-",$request->title).'.'.$file->guessExtension();
                //Delete Previous Image
                File::delete($destinationPath,$category->icon);
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->merge(['icon' => $fileName]);
                $category->icon = $fileName;
            }
        }
        //dd($request->title,$request->icon,$request->body);
        $category->title = $request->title;
        if($request->description){
            $category->description = $request->description;
        }
        if(count($request->gods) > 0){
            $category->gods()->sync($request->gods);
        } else {
            $category->gods()->sync([]);
        }
        $category->save();
        return redirect('/admin/categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //remove image
        /*
        if($category->icon){
            File::delete('img/categories/'.$category->icon);
        }
        */
        $category = Category::withTrashed()
            ->where('id', $id)
            ->first();

        if($category->trashed()){
            $category->restore();
        } else {
            $category->delete();
        }
        return redirect('/admin/categories');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $category = Category::withTrashed()
            ->where('id', $id)
            ->first();
        //remove image

        if($category->icon){
            File::delete('img/categories/'.$category->icon);
        }

        $category->forceDelete();
        return redirect('/admin/categories');
    }
}
