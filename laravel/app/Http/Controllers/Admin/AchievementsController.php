<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Achievement;
use Illuminate\Http\Request;
use File;

class AchievementsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $achievements = Achievement::withTrashed()->orderBy('created_at','desc')->paginate(12);
        return view('backoffice.achievements.index',compact('achievements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.achievements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'title' => 'required'
        ]);

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            if ($request->file('icon')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('icon');
                $destinationPath = 'img/achievements';
                $fileName =str_replace(" ", "-",$request->title).'.'.$file->guessExtension();
                $file->move($destinationPath,$fileName);
                $request->merge(['icon' => $fileName]);
            }
        }

        Achievement::create([
            'title' => $request->title,
            'description' => $request->description,
            'icon' => $fileName,
        ]);
        return redirect('/admin/achievements');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $achievement = Achievement::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.achievements.show',compact('achievement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $achievement = Achievement::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.achievements.edit',compact('achievement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $achievement = Achievement::withTrashed()
            ->where('id', $id)
            ->first();
        $request = request();

        $this->validate($request,[
            'title' => 'required'
        ]);

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            if ($request->file('icon')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('icon');
                $destinationPath = 'img/achievements';

                $fileName = str_replace(" ", "-",$request->title).'.'.$file->guessExtension();
                //Delete Previous Image
                File::delete($destinationPath,$achievement->icon);
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->merge(['icon' => $fileName]);
                $achievement->icon = $fileName;
            }
        }
        //dd($request->title,$request->icon,$request->body);
        $achievement->title = $request->title;
        if($request->body){
            $achievement->body = $request->body;
        }
        $achievement->save();
        return redirect('/admin/achievements');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //remove image
        /*
        if($achievement->icon){
            File::delete('img/achievements/'.$achievement->icon);
        }
        */
        $achievement = Achievement::withTrashed()
            ->where('id', $id)
            ->first();

        if($achievement->trashed()){
            $achievement->restore();
        } else {
            $achievement->delete();
        }
        return redirect('/admin/achievements');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $achievement = Achievement::withTrashed()
            ->where('id', $id)
            ->first();
        //remove image

        if($achievement->icon){
            File::delete('img/achievements/'.$achievement->icon);
        }

        $achievement->forceDelete();
        return redirect('/admin/achievements');
    }
}
