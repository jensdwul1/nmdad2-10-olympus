<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class AdminsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            if(Auth::check()){
                $this->admin = Auth::user()->admin;
                if(!$this->admin->super_admin){
                    return redirect('/admin');
                } else {
                    return $next($request);
                }
            } else {
                return redirect('/login');
            }
        });
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::withTrashed()->orderBy('id','asc')->paginate(25);
        return view('backoffice.admins.index',compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'user' => 'required'
        ]);

        $admin = Admin::create([
            'user_id' => $request->user,
            'super_admin' => $request->superadmin,
        ]);

        return redirect('/admin/admins');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = Admin::withTrashed()
            ->where('id', $id)
            ->first();

        $genders = Profile::GENDER;
        return view('backoffice.admins.show',compact('admin','genders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = Admin::withTrashed()
            ->where('id', $id)
            ->first();

        $users = User::all();
        return view('backoffice.admins.edit',compact('admin','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $admin = Admin::withTrashed()
            ->where('id', $id)
            ->first();
        $request = request();
        $this->validate($request,[
            'user' => 'required'
        ]);
        $admin->user_id = $request->user;
        if($request->superadmin && $request->superadmin == 'on'){
            $admin->super_admin = 1;
            
        } else {
            $admin->super_admin = 0;
        }
        $admin->save();
        return redirect('/admin/admins');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //remove image
        /*
        if($admin->icon){
            File::delete('img/admins/'.$admin->icon);
        }
        */
        $admin = Admin::withTrashed()
            ->where('id', $id)
            ->first();

        if($admin->trashed()){
            $admin->restore();
        } else {
            $admin->delete();
        }
        return redirect('/admin/admins');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $admin = Admin::withTrashed()
            ->where('id', $id)
            ->first();
        //remove image

        $admin->forceDelete();
        return redirect('/admin/admins');
    }
}
