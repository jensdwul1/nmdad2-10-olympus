<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pantheon;

class PantheonsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $pantheons = Pantheon::withTrashed()->orderBy('startDate','asc')->paginate(12);
        return view('backoffice.pantheons.index',compact('pantheons'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.pantheons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'startDate' => 'required',
            'endDate' => 'required',
        ]);

        Pantheon::create([
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
        ]);
        return redirect('/admin/pantheons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pantheon = Pantheon::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.pantheons.show',compact('pantheon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pantheon = Pantheon::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.pantheons.edit',compact('pantheon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $pantheon = Pantheon::withTrashed()
            ->where('id', $id)
            ->first();

        $request = request();
        $this->validate($request,[
            'startDate' => 'required',
            'endDate' => 'required',
        ]);

        $pantheon->startDate = request()->startDate;
        $pantheon->endDate = request()->endDate;

        $pantheon->save();
        return redirect('/admin/pantheons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pantheon = Pantheon::withTrashed()
            ->where('id', $id)
            ->first();
        if($pantheon->trashed()){
            $pantheon->restore();
        } else {
            $pantheon->delete();
        }
        return redirect('/admin/pantheons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $pantheon = Pantheon::withTrashed()
            ->where('id', $id)
            ->first();

        $pantheon->forceDelete();
        return redirect('/admin/pantheons');
    }
}
