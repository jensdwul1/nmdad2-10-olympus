<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\God;
use App\Models\Category;
use File;

class GodsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $gods = God::withTrashed()->orderBy('title','asc')->paginate(12);
        return view('backoffice.gods.index',compact('gods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backoffice.gods.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'title' => 'required'
        ]);

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            if ($request->file('icon')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('icon');
                $destinationPath = 'img/gods';
                $fileName = str_replace(" ", "-",$request->title).'.'.$file->guessExtension();
                $file->move($destinationPath,$fileName);
                $request->merge(['icon' => $fileName]);
            }
        }

        God::create([
            'title' => $request->title,
            'body' => $request->body,
            'icon' => $fileName,
        ]);
        return redirect('/admin/gods');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $god = God::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.gods.show',compact('god'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $god = God::withTrashed()
            ->where('id', $id)
            ->first();
        $categories = Category::orderBy('title','asc')->get();
        $categoriesArray = array_column($god->categories->toArray(), 'id');
        return view('backoffice.gods.edit',compact('god','categories','categoriesArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $god = God::withTrashed()
            ->where('id', $id)
            ->first();
        $request = request();

        $this->validate($request,[
            'title' => 'required'
        ]);

        if ($request->hasFile('icon')) {
            $file = $request->file('icon');
            if ($request->file('icon')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('icon');
                $destinationPath = 'img/gods';

                $fileName = str_replace(" ", "-",$request->title).'.'.$file->guessExtension();
                //Delete Previous Image
                File::delete($destinationPath,$god->icon);
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->merge(['icon' => $fileName]);
                $god->icon = $fileName;
            }
        }
        //dd($request->title,$request->icon,$request->body);
        $god->title = $request->title;
        if($request->body){
            $god->body = $request->body;
        }

        if(count($request->categories) > 0){
            $god->categories()->sync($request->categories);
        } else {
            $god->categories()->sync([]);
        }
        $god->save();
        return redirect('/admin/gods');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //remove image
        /*
        if($god->icon){
            File::delete('img/gods/'.$god->icon);
        }
        */
        $god = God::withTrashed()
        ->where('id', $id)
        ->first();

        if($god->trashed()){
            $god->restore();
        } else {
            $god->delete();
        }
        return redirect('/admin/gods');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $god = God::withTrashed()
            ->where('id', $id)
            ->first();
        //remove image

        if($god->icon){
            File::delete('img/gods/'.$god->icon);
        }

        $god->forceDelete();
        return redirect('/admin/gods');
    }
}
