<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use File;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::withTrashed()->orderBy('name','asc')->paginate(25);
        return view('backoffice.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genders = Profile::GENDER;
        return view('backoffice.users.create',compact('genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'firstName' => 'required',
            'sex' => 'required',
            'picture' => 'required',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'remember_token' => str_random(10),
        ]);

        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            if ($request->file('picture')->isValid()) {
                //$request->file('icon')->store('img');
                $file = $request->file('picture');
                $destinationPath = 'img/users';

                $fileName = $user->id.'_'.$request->firstName.'.'.$file->guessExtension();
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->profile_picture = $fileName;
            }
        }
        $profile = [
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'sex' => $request->sex,
            'dateOfBirth' => $request->dateOfBirth,
            'phone' => $request->phone,
            'country' => $request->country,
            'job' => $request->job,
            'description' => $request->description,
            'profile_picture' => $request->profile_picture,
        ];
        $user->profile()->save($profile);
        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::withTrashed()
            ->where('id', $id)
            ->first();

        $genders = Profile::GENDER;
        return view('backoffice.users.show',compact('user','genders'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::withTrashed()
            ->where('id', $id)
            ->first();

        $genders = Profile::GENDER;
        return view('backoffice.users.edit',compact('user','genders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $user = User::withTrashed()
            ->where('id', $id)
            ->first();
        $request = request();

        $this->validate($request,[
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|exists:users',
            'firstName' => 'required',
            'sex' => 'required',
        ]);
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->password){
            $user->password = bcrypt($request->password);
        }

        if ($request->hasFile('picture')) {
            $file = $request->file('picture');
            if ($request->file('picture')->isValid()) {
                //$request->file('picture')->store('img');
                $file = $request->file('picture');
                $destinationPath = 'img/users';

                $fileName = $user->id.'_'.$request->firstName.'.'.$file->guessExtension();
                //Delete Previous Image
                File::delete($destinationPath,$user->profile->profile_picture);
                //Move the new one in
                $file->move($destinationPath,$fileName);
                $request->merge(['picture' => $fileName]);
                $user->profile->profile_picture = $fileName;
            }
        }

        $user->profile->firstName = $request->firstName;
        $user->profile->lastName = $request->lastName;
        $user->profile->sex = $request->sex;
        $user->profile->dateOfBirth = $request->dateOfBirth;
        $user->profile->phone = $request->phone;
        $user->profile->country = $request->country;
        $user->profile->job = $request->job;
        $user->profile->description = $request->description;
        $user->profile->save();
        $user->save();
        //dd($user);
        return redirect('/admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //remove image
        /*
        if($user->icon){
            File::delete('img/users/'.$user->icon);
        }
        */
        $user = User::withTrashed()
            ->where('id', $id)
            ->first();

        if($user->trashed()){
            $user->restore();
        } else {
            $user->delete();
        }
        return redirect('/admin/users');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $user = User::withTrashed()
            ->where('id', $id)
            ->first();
        //remove image

        $user->forceDelete();
        return redirect('/admin/users');
    }
}
