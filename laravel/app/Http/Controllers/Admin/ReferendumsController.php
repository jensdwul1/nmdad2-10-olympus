<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Referendum;
use App\Models\User;
use App\Models\Profile;
use App\Models\Category;
use Illuminate\Http\Request;

class ReferendumsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $referendums = Referendum::withTrashed()->orderBy('startDate','asc')->paginate(12);
        return view('backoffice.referendums.index',compact('referendums'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function actives(){
        $referendums = Referendum::where('active','1')->orderBy('startDate','asc')->paginate(12);
        return view('backoffice.referendums.index',compact('referendums'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profiles = Profile::orderBy('firstName','asc')->get();
        return view('backoffice.referendums.create',compact('referendum','profiles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $request = request();
        $this->validate($request,[
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'user' => 'required'
        ]);
        Referendum::create([
            'title' => $request->title,
            'description' => $request->description,
            'startDate' => $request->startDate,
            'endDate' => $request->endDate,
            'user_id' => $request->user,
        ]);
        return redirect('/admin/referendums');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $referendum = Referendum::withTrashed()
            ->where('id', $id)
            ->first();
        return view('backoffice.referendums.show',compact('referendum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $referendum = Referendum::withTrashed()
            ->where('id', $id)
            ->first();
        $profiles = Profile::orderBy('firstName','asc')->get();
        $categories = Category::orderBy('title','asc')->get();
        $categoriesArray = array_column($referendum->categories->toArray(), 'id');
        return view('backoffice.referendums.edit',compact('referendum','profiles','categories','categoriesArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $referendum = Referendum::withTrashed()
            ->where('id', $id)
            ->first();

        $request = request();
        $this->validate($request,[
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'user' => 'required'
        ]);

        $referendum->title = request()->title;
        $referendum->startDate = request()->startDate;
        $referendum->endDate = request()->endDate;
        $referendum->user_id = request()->user;

        if(count($request->categories) > 0){
            $referendum->categories()->sync($request->categories);
        } else {
            $referendum->categories()->sync([]);
        }

        $referendum->save();
        return redirect('/admin/referendums');
    }

    /**
     * Active toggle the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $referendum = Referendum::withTrashed()
            ->where('id', $id)
            ->first();
        if($referendum->active){
            $referendum->active = 0;
        } else {
            $referendum->active = 1;
        }
        $referendum->save();
        return redirect('/admin/referendums');

    }

    public function check(){
        $date = date('Y-m-d');
        $referendums = Referendum::where('active','1')->where('endDate','<',$date)->update(['active' => 0]);
        //dd($referendums);
        return redirect('/admin/referendums');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $referendum = Referendum::withTrashed()
            ->where('id', $id)
            ->first();
        if($referendum->trashed()){
            $referendum->restore();
        } else {
            $referendum->delete();
        }
        return redirect('/admin/referendums');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function crush($id)
    {
        $referendum = Referendum::withTrashed()
            ->where('id', $id)
            ->first();

        $referendum->forceDelete();
        return redirect('/admin/referendums');
    }
}
