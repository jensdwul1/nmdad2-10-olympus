<?php

namespace App\Http\Controllers\Core;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Referendum;
use App\Models\Election;
use App\Models\Vote;
use App\Models\God;
use App\Models\User;
use App\Models\Pantheon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $month = date('Y-m-t');

        $cur_pantheon = Pantheon::where('endDate', $month)->first();
        return view('frontoffice.home.index',compact('cur_pantheon'));
    }
}
