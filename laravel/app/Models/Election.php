<?php

namespace App\Models;

class Election extends Model
{
    public function apiFetch($id){
        return Election::
            with('votes')
            ->with('candidates')
            ->with('god')
            ->find($id);
    }
    static function apiFetchAll(){
        return Election::
            with('votes')
            ->with('candidates')
            ->with('candidates.user.profile')
            ->with('god')
            ->whereDate('endDate', date('Y-m-t'))
            ->orderBy('startDate','desc')
            ->get();
    }
    public function votes(){
        return $this->hasMany('App\Models\Vote');
    }
    public function candidates()
    {
        return $this->hasMany('App\Models\Candidate');
    }
    public function god()
    {
        return $this->belongsTo('App\Models\God');
    }
}
