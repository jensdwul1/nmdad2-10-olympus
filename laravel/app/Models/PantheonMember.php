<?php

namespace App\Models;

class PantheonMember extends Model
{

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function pantheon()
    {
        return $this->belongsTo('App\Models\Pantheon');
    }
    public function god()
    {
        return $this->belongsTo('App\Models\God');
    }
}
