<?php

namespace App\Models;

class Achievement extends Model
{

    public function users(){
        return $this->belongsToMany('App\Models\User')->withTimestamps();
    }
}
