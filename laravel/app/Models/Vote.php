<?php

namespace App\Models;

class Vote extends Model
{
    public function apiFetch($id){
        return Vote::
        with('election')
            ->with('referendum')
            ->with('user')
            ->find($id);
    }
    public function apiFetchByUser($id){
        return Vote::
            where('user_id',$id)
            ->get();
    }
    static function apiFetchAll(){
        return Vote::
        with('election')
            ->with('referendum')
            ->with('user')
            ->get();
    }
    //
    public function referendum()
    {
        return $this->belongsTo('App\Models\Referendum');
    }
    public function election()
    {
        return $this->belongsTo('App\Models\Election');
    }
    public function candidate()
    {
        return $this->belongsTo('App\Models\Candidate');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
