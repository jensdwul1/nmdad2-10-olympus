<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function apiFetch($id){
        return User::
            with('profile')
            ->with('achievements')
            ->with('members')
            ->with('members.pantheon')
            ->with('members.god')
            ->with('candidates')
            ->with('tallies')
            ->find($id);
    }
    public function profile(){
        return $this->hasOne('App\Models\Profile');
    }
    public function tallies(){
        return $this->hasMany('App\Models\Vote');
    }
    public function candidates(){
        return $this->hasMany('App\Models\Candidate');
    }
    public function members()
    {
        return $this->hasMany('App\Models\PantheonMember');
    }
    public function achievements(){
        return $this->belongsToMany('App\Models\Achievement')->withTimestamps();
    }
    public function referendums(){
        return $this->hasMany('App\Models\Referendum');
    }
    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }
    public function admin()
    {
        return $this->hasOne('App\Models\Admin');
    }
}
