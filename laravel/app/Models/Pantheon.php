<?php

namespace App\Models;

class Pantheon extends Model
{

    public function apiFetch($id){
        return Pantheon::
            with('members')
            ->with('members.god')
            ->with('members.user')
            ->with('members.user.profile')
            ->find($id);
    }
    static function apiFetchAll(){
        return Pantheon::
            with('members')
            ->with('members.god')
            ->with('members.user')
            ->with('members.user.profile')
            ->whereDate('startDate','<=', date('Y-m-d'))
            ->orderBy('startDate','desc')
            ->get();
    }
    public function members()
    {
        return $this->hasMany('App\Models\PantheonMember');
    }
}
