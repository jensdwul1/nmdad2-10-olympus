<?php

namespace App\Models;


class Referendum extends Model
{

    public function scopeCurrent($query){
        return $query->whereDate('endDate','>=', date('Y-m-d'))->whereDate('startDate','<=', date('Y-m-d'));
    }
    public function apiFetch($id){
         $ref = Referendum::with('votes')
            ->with('user')
            ->with('user.profile')
            ->with(['comments' => function ($query) { $query->with('user.profile')->orderBy('parent_id'); }])
            ->with('categories')
            ->find($id);
         return $ref;
    }
    static function apiFetchAll(){
         return Referendum::with('votes')
            ->with('user')
            ->with('user.profile')
            ->with(['comments' => function ($query) { $query->whereNull('parent_id'); }])
            ->with('comments.children')
            ->with('categories')
            ->get();
    }
    public function godVotes(){
        return $this->hasMany('App\Models\GodVote');
    }
    public function votes(){
        return $this->hasMany('App\Models\Vote');
    }
    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }
    public function categories(){
        return $this->belongsToMany('App\Models\Category')->withTimestamps();
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
