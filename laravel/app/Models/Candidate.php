<?php

namespace App\Models;

class Candidate extends Model
{


    static function apiFetchAll(){
        return Candidate::
        with('election')
            ->with('user.profile')
            ->with('election.god')
            ->whereDate('election.endDate', date('Y-m-t'))
            ->get();
    }

    public function election()
    {
        return $this->belongsTo('App\Models\Election');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function votes(){
        return $this->hasMany('App\Models\Vote');
    }
}
