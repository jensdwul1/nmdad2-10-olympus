<?php

namespace App\Models;

class Category extends Model
{

    public function apiFetch($id){
        $ref = Category::with('gods')
            ->with('referendums')
            ->find($id);
        return $ref;
    }
    public function gods(){
        return $this->belongsToMany('App\Models\God')->withTimestamps();
    }
    public function referendums(){
        return $this->belongsToMany('App\Models\Referendum')->withTimestamps();
    }
}
