<?php

namespace App\Models;

class Comment extends Model
{
    public function apiFetch($id){
        $ref = Comment::with('user')
            ->with('user.profile')
            ->with('children')
            ->with('referendum')
            ->find($id);
        return $ref;
    }
    public function apiFetchByReferendum($id){
        return Comment::with('user.profile')
            ->where('referendum_id',$id)
            ->groupBy('parent_id')
            ->get();
    }
    static function apiFetchAll(){
        return Comment::with('user')
            ->with('user.profile')
            ->with('children')
            ->with('referendum')
            ->groupBy('parent_id')
            ->get();
    }
    public function parent(){
        return $this->belongsTo('App\Models\Comment','parent_id');
    }
    public function children(){
        return $this->hasMany('App\Models\Comment','parent_id');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    public function referendum()
    {
        return $this->belongsTo('App\Models\Referendum');
    }
}
