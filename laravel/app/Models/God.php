<?php

namespace App\Models;

class God extends Model
{
    public function apiFetch($id){
        return God::
            with('categories')
            ->with('elections')
            ->find($id);
    }
    public function categories(){
        return $this->belongsToMany('App\Models\Category')->withTimestamps();
    }
    public function elections(){
        return $this->hasMany('App\Models\Election');
    }
}
