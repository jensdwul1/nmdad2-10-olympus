<?php

namespace App\Models;


class Admin extends Model
{

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
