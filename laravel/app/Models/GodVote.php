<?php

namespace App\Models;
use DB;
use Hash;
use CreateGodVotesTable;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;


class GodVote extends Model
{
    //
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'details' => 'array', // Casts array to JSON and vice versa.
    ];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = CreateGodVotesTable::PK;
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        /**
         * Register a creating model event with the dispatcher.
         */
        static::creating(function(GodVote $godVote) {
            // Create Universally Unique Identifier.
            do {
                $uuid = Uuid::uuid4()->toString(); // @see https://github.com/ramsey/uuid
            } while (DB::table(CreateGodVotesTable::TABLE)
                ->select(CreateGodVotesTable::PK)
                ->where(CreateGodVotesTable::PK, $uuid)
                ->exists());
            $godVote->uuid = $uuid;
            // Create Checksum, assume password
            $data = $godVote->getAttributes();
            ksort($data);
            $value = hash('sha512', (json_encode($data)).$godVote->checksum);
            // \Log::debug([$data, $value]);
            $godVote->checksum = bcrypt($value);
        });
    }

    public function referendum()
    {
        return $this->belongsTo('App\Models\Referendum');
    }
}
