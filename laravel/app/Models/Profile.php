<?php

namespace App\Models;

class Profile extends Model
{
    const GENDER = [
        0 => 'Unknown',
        1 => 'Male',
        2 => 'Female',
        9 => 'Not Applicable'
    ];
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
