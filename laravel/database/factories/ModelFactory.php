<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/


//USER RELATED SEEDING

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->userName,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Profile::class, function (Faker\Generator $faker) {
    return [
        'firstName' => $faker->firstName,
        'lastName' => $faker->optional($weight = 0.9)->lastName,
        'sex' => $faker->randomElement($array = array (0,1,2,9)),
        'dateOfBirth' => $faker->optional($weight = 0.9)->dateTimeBetween('-40 years', '-18 years'),
        'phone' => $faker->optional($weight = 0.9)->phoneNumber,
        'country' => $faker->optional($weight = 0.9)->country,
        'job' => $faker->optional($weight = 0.9)->jobTitle,
        'description' => $faker->optional($weight = 0.9)->sentence(20),
        'profile_picture' => 'lego/'.$faker->numberBetween(0,8).'.jpg',
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Admin::class, function (Faker\Generator $faker) {
    return [
        'super_admin' => 0,
    ];
});

// ACHIEVEMENT SEEDING

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Achievement::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->sentence(20),
        'icon' => $faker->imageUrl($width = 100, $height = 100),
    ];
});
// GODS SEEDING

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\God::class, function (Faker\Generator $faker) {
    $name = $faker->name;
    return [
        'title' => $name,
        'body' => $faker->sentence(20),
        'icon' => $name.'.png',
    ];
});

// ELECTION SEEDING
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Election::class, function (Faker\Generator $faker) {
    $month = $faker->month();
    $year = 2017;
    $daysInMonth = round((mktime(0, 0, 0, $month+1, 1, $year) - mktime(0, 0, 0, $month, 1, $year)) / 86400);
    return [
        'title' => 'The Ascendance of '.$faker->word,
        'god_id' =>  $faker->numberBetween(1,12),
        'startDate' => $year.'-'.$month.'-01',
        'endDate' => $year.'-'.$month.'-'.$daysInMonth,
        'result' => 0
    ];
});

// CANDIDATE SEEDING
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Candidate::class, function (Faker\Generator $faker) {
    return [
        'election_id' => $faker->numberBetween(1,144),
        'user_id' =>  $faker->numberBetween(2,51),
    ];
});

// REFERENDUM SEEDING
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Referendum::class, function (Faker\Generator $faker) {
    $startDate = $faker->dateTimeBetween('now','+6 month');
    return [
        'title' => $faker->sentence,
        'description' => $faker->sentence(10),
        'user_id' =>  $faker->numberBetween(2,52),
        'startDate' => $startDate->format('Y-m-d'),
        'endDate' => $faker->dateTimeBetween($startDate,'+6 month')->format('Y-m-d'),
        'result' => 0,
        'active' => 1,
    ];
});

// COMMENT SEEDING
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Comment::class, function (Faker\Generator $faker) {
    return [
        'body' => $faker->sentence(rand(1,20)),
        'user_id' =>  $faker->numberBetween(2,52),
        'referendum_id' =>  $faker->numberBetween(1,50),
    ];
});

// PANTHEON SEEDING
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Pantheon::class, function (Faker\Generator $faker) {
    $month = $faker->month();
    $year = 2017;
    $daysInMonth = round((mktime(0, 0, 0, $month+1, 1, $year) - mktime(0, 0, 0, $month, 1, $year)) / 86400);
    return [
        'startDate' => $year.'-'.$month.'-01',
        'endDate' => $year.'-'.$month.'-'.$daysInMonth,
    ];
});

// PANTHEON MEMBER SEEDING
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\PantheonMember::class, function (Faker\Generator $faker) {
    return [
        'user_id' =>  $faker->numberBetween(2,52),
        'pantheon_id' =>  $faker->numberBetween(1,24),
        'god_id' =>  $faker->numberBetween(1,12),
    ];
});
// CATEGORIES SEEDING

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    $name = $faker->name;
    return [
        'title' => $name,
        'description' => $faker->sentence(20),
        'icon' => str_replace(" ", "-",$name).'.png',
    ];
});
// VOTES SEEDING

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Vote::class, function (Faker\Generator $faker) {
    $name = $faker->name;
    $election = null;
    $candidate = null;
    $referendum = null;
    $booli = rand(0,1);
    if($booli){
        $election = $faker->numberBetween(1,144);
        $candidates = compact(App\Models\Election::with('candidates')->find($election));
        if(count($candidates) > 0){
            $rando = rand(0,count($candidates) - 1);
            //dd($candidates);
            $candidate = $candidates[$rando]->id;
        }
    } else {
        $referendum = $faker->numberBetween(1,50);
    }
    return [
        'user_id' =>  $faker->numberBetween(2,52),
        'election_id' => $election,
        'candidate_id' => $candidate,
        'referendum_id' => $referendum,
        'value' => rand(0,1),
    ];
});