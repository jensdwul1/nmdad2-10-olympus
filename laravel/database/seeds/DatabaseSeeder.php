<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(GodsTableSeeder::class);
        $this->call(AchievementsTableSeeder::class);
        $this->call(ElectionsTableSeeder::class);
        $this->call(ReferendumsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PantheonsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
    }
}
