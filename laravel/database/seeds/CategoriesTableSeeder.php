<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoriesList = [
            [
                'title' => 'Art',
                'description' => '',
                'gods' => [2,7]
            ],[
                'title' => 'Beauty',
                'description' => '',
                'gods' => [1,8]
            ],[
                'title' => 'Communication',
                'description' => '',
                'gods' => [9]
            ],[
                'title' => 'Crime',
                'description' => '',
                'gods' => [3,6,9]
            ],[
                'title' => 'Death',
                'description' => '',
                'gods' => [3,6]
            ],[
                'title' => 'Earth',
                'description' => '',
                'gods' => [6,11]
            ],[
                'title' => 'Environment',
                'description' => '',
                'gods' => [4,11]
            ],[
                'title' => 'Family',
                'description' => '',
                'gods' => [1,4,8,10]
            ],[
                'title' => 'Industry',
                'description' => '',
                'gods' => [7,9]
            ],[
                'title' => 'Knowledge',
                'description' => '',
                'gods' => [5]
            ],[
                'title' => 'Life',
                'description' => '',
                'gods' => [1,4,8]
            ],[
                'title' => 'Literature',
                'description' => '',
                'gods' => [5,8]
            ],[
                'title' => 'Love',
                'description' => '',
                'gods' => [1,8]
            ],[
                'title' => 'Music',
                'description' => '',
                'gods' => ['2']
            ],[
                'title' => 'Nature',
                'description' => '',
                'gods' => [4,11]
            ],[
                'title' => 'Ocean',
                'description' => '',
                'gods' => [11]
            ],[
                'title' => 'Politics',
                'description' => '',
                'gods' => [8,9,10]
            ],[
                'title' => 'Science',
                'description' => '',
                'gods' => [2,5,7]
            ],[
                'title' => 'War',
                'description' => '',
                'gods' => [3,5]
            ],[
                'title' => 'Weather',
                'description' => '',
                'gods' => [2,4,11]
            ]
        ];
        foreach($categoriesList as $key => $category){
            $categori = factory(App\Models\Category::class)->create([
                'title' => $category['title'],
                'description' => $category['description'],
                'icon' => str_replace(" ", "-",$category['title']).'.png',
            ]);
            $categori->gods()->attach($category['gods']);
            $refCount = rand(1,20);
            $referendi = [];
            for($i = 0; $i < $refCount; ++$i){
                $numb = rand(1,50);
                if(!in_array($numb,$referendi)){
                    $referendi[] = $numb;
                }
            }
            $categori->referendums()->attach($referendi);
        }
    }
}
