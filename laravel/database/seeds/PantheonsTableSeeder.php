<?php

use Illuminate\Database\Seeder;
use App\Models\God;

class PantheonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $start = new DateTime('2017-01-01');
        $interval = new DateInterval('P1M');
        $end = new DateTime('2019-01-01');
        $period = new DatePeriod($start, $interval, $end);
        $gods = God::all();

        foreach ($period as $dt) {
            $month = $dt->format('m');
            $year = $dt->format('Y');
            $daysInMonth = $dt->format('t');
            $pantheon = factory(App\Models\Pantheon::class)->create([
                'startDate' => $year.'-' . $month . '-01',
                'endDate' => $year.'-' . $month . '-' . $daysInMonth,
            ]);
            if(strtotime($pantheon->startDate) <= strtotime(date('Y-m-d'))){
                $numbers = range(2, 52);
                shuffle($numbers);
                $usersArray = array_slice($numbers, 0, 12);
                foreach ($gods as $key => $god) {
                    factory(App\Models\PantheonMember::class)->create([
                        'pantheon_id' => $pantheon->id,
                        'god_id' => $god->id,
                        'user_id' => $usersArray[$key]
                    ]);
                }
            }
        }
    }
}
