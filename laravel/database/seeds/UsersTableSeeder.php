<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deleted = App\Models\User::create([
            'id' => 0,
            'name' => 'Redacted',
            'email' => 'redacted@olympus.com',
            'password' => bcrypt('Testing'),
        ]);
        $admin = factory(App\Models\User::class)->create([
            'name' => 'Jens',
            'email' => 'jdw.jensdewulf@gmail.com',
            'password' => bcrypt('Testing'),
        ]);
        $admin->profile()->save(factory(App\Models\Profile::class)->make(['firstName'=> 'Jens','lastName'=>'De Wulf','sex'=>1,'dateOfBirth'=>'1992-04-13','profile_picture'=>'2_Jens.png']));
        $admin->admin()->save(factory(App\Models\Admin::class)->make(['super_admin'=>1]));


        $users = factory(App\Models\User::class, 50)->create()->each(function ($u) {
            //Create a Profile for User
            $u->profile()->save(factory(App\Models\Profile::class)->make());
            $rand = rand(0, 9);

            // Create a random amount of admins for managing the system
            if($rand <= 1){
                $u->admin()->save(factory(App\Models\Admin::class)->make());
            }
        });
    }
}
