<?php

use Illuminate\Database\Seeder;

class AchievementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $achievementsList = [
            [
                'name' => 'Ascendance',
                'body' => 'Has ascended.',
            ],[
                'name' => 'Lineage',
                'body' => 'Has ascended to the same godhood twice.',
            ],[
                'name' => 'Divinity',
                'body' => 'Has embodied more than a single god.',
            ],[
                'name' => 'Banished',
                'body' => 'Has been banned.',
            ],[
                'name' => 'Hand of Zeus',
                'body' => 'This is a secret achievement that only the best can get.',
            ],[
                'name' => 'God of the people',
                'body' => 'Won the ascension with +60% majority.',
            ]
        ];
        foreach($achievementsList as $key => $achievement){
            $achievement = factory(App\Models\Achievement::class)->create([
                'title' => $achievement['name'],
                'description' => $achievement['body'],
                'icon' => str_replace(" ", "-",$achievement['name']).'.png',
            ]);
            $userCount = rand(1,20);
            $users = [];
            for($i = 0; $i < $userCount; ++$i){
                $numb = rand(2,52);
                if(!in_array($numb,$users)){
                    $users[] = $numb;
                }
            }
            $achievement->users()->attach($users);
        }
    }
}
