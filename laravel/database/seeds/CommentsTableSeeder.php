<?php

use Illuminate\Database\Seeder;
use App\Models\Referendum;
use App\Models\Comment;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $referendums = Referendum::all();
        $comments = factory(App\Models\Comment::class, 100)->create()->each(function ($com) {
            //Randomly create child comments
            $rand = rand(0,1);
            if($rand){
                factory(App\Models\Comment::class,rand(1,5))->create(['referendum_id' => $com->referendum_id,'parent_id' => $com->id]);
                //Could loop once more for lvl2 child comments
            }
        });
    }
}
