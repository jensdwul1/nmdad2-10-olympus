<?php

use Illuminate\Database\Seeder;

class GodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $godsList = [
            [
                'name' => 'Aphrodite',
                'body' => 'Aphrodite was the goddess of love, desire and beauty. Apart from her natural beauty, she also had a magical girdle that compelled everyone to desire her.',
            ],[
                'name' => 'Apollo',
                'body' => 'Apollo was the son of Zeus and Leto, twin brother of Artemis. He was the god of music, and he is often depicted playing a golden lyre. He was also known as the Archer, far shooting with a silver bow; the god of healing, giving the science of medicine to man; the god of light; and the god of truth. One of Apollo\'s most important daily tasks was to harness his four-horse chariot, in order to move the Sun across the sky.',
            ],[
                'name' => 'Ares',
                'body' => 'Ares was the god of war, and son of Zeus and Hera. He represented the raw violence and untamed acts that occured in wartime, in contrast to Athena, who was a symbol of tactical strategy and military planning.',
            ],[
                'name' => 'Artemis',
                'body' => 'Artemis was the goddess of chastity, virginity, the hunt, the moon, and the natural environment.',
            ],[
                'name' => 'Athena',
                'body' => 'Athena was the Greek virgin goddess of reason, intelligent activity, arts and literature. She was the daughter of Zeus; her birth is unique in that she did not have a mother. Instead, she sprang full grown and clad in armour from Zeus\' forehead.',
            ],[
                'name' => 'Hades',
                'body' => 'Hades was the brother of Zeus and Poseidon. After the overthrow of their father, Cronus, he drew lots with them to share the universe. He drew poorly, which resulted in becoming lord of the underworld and ruler of the dead. Nevertheless, he was not considered to be death itself, as this was a different god, called Thanatos. Greedy like his brother Poseidon, he was mainly interested in increasing his subjects, and anyone whose deeds resulted in people dying was favoured by him. The Erinnyes (the Furies) were welcomed guests in his kingdom.',
            ],[
                'name' => 'Hephaestus',
                'body' => 'Hephaestus was the Greek god of blacksmiths, sculptors, metallurgy, fire and volcanoes; thus, he is symbolised with a hammer, an anvil and a pair of tongs.',
            ],[
                'name' => 'Hera',
                'body' => 'Hera was Zeus\' wife and sister, and was raised by the Titans Oceanus and Tethys. She was the supreme goddess, patron of marriage and childbirth, having a special interest in protecting married women. Her sacred animals were the cow and the peacock, and she favoured the city of Argos.',
            ],[
                'name' => 'Hermes',
                'body' => 'Hermes was the Greek god of commerce, son of Zeus and Maia. Quick acting and cunning, he was able to move swiftly between the world of man and the world of gods, acting as a messenger of the gods and the link between mortals and the Olympians.',
            ],[
                'name' => 'Hestia',
                'body' => 'Hestia was the goddess of the hearth, family, and domestic life. She was not worshipped publicly, which is evident by the lack of temples and shrines attributed to her; this comes in contrast to the Roman equivalent goddess Vesta, who represented the public hearth. Her name meant both a house and a hearth, symbolising the home and its residents.',
            ],[
                'name' => 'Poseidon',
                'body' => 'Poseidon is the god of the sea and protector of all aquatic features. Brother of Zeus and Hades, after the overthrow of their father, Cronus, he drew lots with them to share the universe. He ended up becoming lord of the sea. He was widely worshipped by seamen. He married Amphitrite, one of the granddaughters of the Titan Oceanus.',
            ],[
                'name' => 'Zeus',
                'body' => 'Zeus was the god of the sky and ruler of the Olympian gods. He overthrew his father, Cronus, and then drew lots with his brothers Poseidon and Hades, in order to decide who would succeed their father on the throne. Zeus won the draw and became the supreme ruler of the gods, as well as lord of the sky and rain. His weapon was a thunderbolt which he hurled at those who displeased or defied him, especially liars and oathbreakers. He was married to Hera but often tested her patience, as he was infamous for his many affairs.',
            ],
        ];
        foreach($godsList as $key => $god){
            $god = factory(App\Models\God::class)->create([
                'title' => $god['name'],
                'body' => $god['body'],
                'icon' => $god['name'].'.png',
            ]);
        }
    }
}
