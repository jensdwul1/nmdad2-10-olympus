<?php

use Illuminate\Database\Seeder;

use App\Models\God;

class ElectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $gods = God::all();
        $start = new DateTime('2017-02-01');
        $interval = new DateInterval('P1M');
        $end = new DateTime('2018-02-01');
        $period = new DatePeriod($start, $interval, $end);

        foreach ($period as $dt) {
            $month = $dt->format('m');
            $year = 2017;
            $daysInMonth = $dt->format('t');
            foreach ($gods as $god) {
                $election = factory(App\Models\Election::class)->create([
                    'god_id' => $god->id,
                    'title' => 'The Ascendance of '.$god->title. ' - '.$dt->format('F'),
                    'startDate' => '2017-' . $month . '-01',
                    'endDate' => '2017-' . $month . '-' . $daysInMonth,
                    'result' => 0
                ]);
                factory(App\Models\Candidate::class,rand(1,5))->create(['election_id' => $election->id])->each(function ($com) {
                    //Randomly create votes
                    $rando = rand(1,25);
                    $numbers = range(2,52);
                    shuffle($numbers);
                    $usersArray = array_slice($numbers, 0, $rando);
                    for($i = 0; $i < (count($usersArray)); $i++) {
                        factory(App\Models\Vote::class)->create(['referendum_id' =>null,'election_id' => $com->election_id,'candidate_id' => $com->id,'value' => rand(0,1),'user_id' => $usersArray[$i]]);
                    }
                });
            }

        }
    }
}
