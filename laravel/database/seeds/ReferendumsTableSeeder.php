<?php

use Illuminate\Database\Seeder;

class ReferendumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $referendums = factory(App\Models\Referendum::class, 50)->create()->each(function ($com) {
            //Randomly create votes
            $rando = rand(1,25);
            $numbers = range(2, 52);
            shuffle($numbers);
            $usersArray = array_slice($numbers, 0, $rando);
            for($i = 0; $i < (count($usersArray)); $i++) {
                factory(App\Models\Vote::class)->create(['referendum_id' =>$com->id,'election_id' => null,'candidate_id' => null,'value' => 1,'user_id' => $usersArray[$i]]);
            }

        });
    }
}
