<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePantheonMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pantheon_members', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('pantheon_id')->references('id')->on('pantheons')->onDelete('cascade');
            $table->foreign('god_id')->references('id')->on('gods')->onDelete('cascade');

            $table->integer('user_id')->unsigned();
            $table->integer('pantheon_id')->unsigned();
            $table->integer('god_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pantheon_members');
    }
}
