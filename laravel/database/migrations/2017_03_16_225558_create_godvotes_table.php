<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGodVotesTable extends Migration
{
    const TABLE = 'godvotes';
    const PK = 'uuid';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(static::TABLE, function (Blueprint $table) {
            // Meta Data
            $table->uuid(static::PK)
                ->primary(static::PK);
            $table->string('checksum')
                ->nullable();
            // Foreign Keys
            $table->integer('referendum_id')->unsigned()->nullable();
            $table->foreign('referendum_id')->references('id')->on('referendums')->onDelete('cascade');
            // Data
            $table->json('details');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(static::TABLE);
    }
}