/**
 * Created by jdwje on 12-5-2017.
 */

(function () {

    /**
     * Install plugin
     * @param Vue
     * @param axios
     */

    function plugin(Vue, authentication) {

        if (plugin.installed) {
            return
        }
        plugin.installed = true

        if (!authentication) {
            console.error('You have to install authentication')
            return
        }

        Vue.authentication = authentication

        Object.defineProperties(Vue.prototype, {

            authentication: {
                user: authentication.user,
                login(creds) {
                    return authentication.login(creds)
                },
                register(creds) {
                    return authentication.register(creds)
                },
                update(creds) {
                    return authentication.update(creds)
                },
                logout() {
                    return authentication.logout()
                },
                checkAuth() {
                    return authentication.checkAuth()
                },
            },

        })
    }

    if (typeof exports == "object") {
        module.exports = plugin
    } else if (typeof define == "function" && define.amd) {
        define([], function(){ return plugin })
    } else if (window.Vue && window.authentication) {
        Vue.use(plugin, window.authentication)
    }

})();