
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./vendor/bootstrap');
require('./vendor/materialize.min');
require('./vendor/what-input');

/* XHR Axios implementation in Vue */
import axios from 'axios';
import VueAxios from 'vue-axios';
Vue.use(VueAxios, axios);

window.shared = {
    api:'http://wirefiddle.local/api/',
    session: {
        user: null,
    }
}

window.shared.install = function(){
    Object.defineProperty(Vue.prototype, '$shared', {
        get () { return window.shared }
    })
}

/* Official Vue Router */
import VueRouter from 'vue-router';
Vue.use(VueRouter);

/* IMPORT Routes because cleaner */
import routes from './routes';
const router = new VueRouter({
    routes
});
window.router = router;

import authentication from './authentication';
import VueAuth from './vendor/VueAuthentication';
Vue.use(VueAuth, authentication);
Vue.authentication.checkAuth();

/* Page Components */
Vue.component('header-item', require('./components/Header.vue'));
Vue.component('navigation', require('./components/Nav.vue'));
Vue.component('footer-item', require('./components/Footer.vue'));

const app = new Vue({
    el: '#app',
    router,
    data: {
        page: {
            name: 'Home',
            path: '/',
            pageIsActive(link){
                return (page.path == link.path);
            },
            setPage(newPage){
                page.name = newPage.name;
                page.path = newPage.path;
            }
        },
        currentRoute: window.location.pathname,
    },
    computed: {

    },
}).$mount('#app');

