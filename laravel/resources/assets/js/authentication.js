const API_URL = 'http://nmdad2.10-olympus.local/api/';
export default {
    // User object will let us check authentication status
    user: {
        user: [],
        authenticated: false,
        errormessage: ""
    },
    login(creds){
        let self = this;
        //console.log('LOGGING IN',creds);
        let login = window.axios.post(API_URL + 'login', creds).then(response => {
            this.user.user = response.data;
            if(this.user.user[0].id === undefined || this.user.user[0].id === null){
                console.log('Email or password incorrect.');
                return this.user.errormessage =  "ERROR - Email or password incorrect.";
            }
            else {
                localStorage.setItem('olympusToken', this.user.user[0].id);
                this.user.authenticated = true;
                this.checkAuth();
                window.router.push({path:'vote'})
                //console.log('LOGIN SUCCESSFUL',this.user);
            }
        } )
        return login;
    },
    register(creds){
        //console.log('REGISTERING',creds);
        let register = window.axios.post(API_URL + 'register', creds).then(response => {
            this.user.user = response.data;
            if(this.user.user.id === undefined || this.user.user.id === null){
                console.log('Email or password incorrect.');
                return this.user.errormessage =  "ERROR - Invalid Registration.";
            }
            else {
                localStorage.setItem('olympusToken', this.user.user.id);
                this.user.authenticated = true;
                this.checkAuth();
                window.router.push({path:'vote'})
                //console.log('LOGIN SUCCESSFUL',this.user);
            }
        } )
        return register;
    },
    update(creds){
        console.log('You tried to update your user data');
    },
    // To log out, we just need to remove the token
    logout() {
        console.log('Logging out');
        localStorage.removeItem('olympusToken');
        sessionStorage.removeItem('olympusUser');
        localStorage.removeItem('olympusToken');
        this.user.authenticated = false;
        this.user.projects = [];
        this.user.user = [];
        window.router.push({path:'pantheons'})
    },
    checkAuth() {
        let jwt = localStorage.getItem('olympusToken');
        let self = this;
        //console.log(jwt)
        if(jwt) {
            this.user.authenticated = true;
            //console.log('User is authenticated',this.user);
            if(this.user.user[0] == null){
                let localUser = sessionStorage.getItem('olympusUser');

                if(localUser){
                    let data = JSON.parse(localUser);
                    self.user.user = data.user;
                    //console.log('Converted the stored json and turned it into app data',self.user);
                } else{
                    //console.log('Fetch and Populate the user data');
                    window.axios.get(API_URL + 'users/'+jwt).then(response => {
                        //console.log('User',response.data);
                        this.user.user = response.data;
                        sessionStorage.setItem('olympusUser',JSON.stringify(self.user));
                    });
                }
            }
        }
        else {
            this.user.authenticated = false;
        }
    },
}

