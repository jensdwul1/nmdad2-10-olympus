/**
 * Created by Jens on 30/04/2017.
 */

export default [
    {
        path:'/',
        component: require('./pages/Pantheons')
    },
    {
        path:'/vote',
        component: require('./pages/Vote')
    },
    {
        path:'/pantheons',
        component: require('./pages/Pantheons')
    },
    {
        path:'/ascend',
        component: require('./pages/Ascend')
    },
    {
        path:'/requests',
        component: require('./pages/Requests')
    },
    {
        path:'/requests',
        component: require('./pages/Requests')
    },
    {
        path:'/requests/new',
        component: require('./pages/NewRequest')
    },
    {
        path:'/requests/:id',
        component: require('./pages/Request'),
        props: true
    },
    {
        path:'/login',
        component: require('./pages/Auth/Login')
    },
    {
        path:'/register',
        component: require('./pages/Auth/Register')
    },
    {
        path:'/profile',
        component: require('./pages/Auth/Profile')
    },
    {
        path:'/profile/:id',
        component: require('./pages/Auth/Profile'),
        props: true
    },
    {
        path:'/info',
        component: require('./pages/Info')
    },
    {
        path:'/gods',
        component: require('./pages/Gods')
    },
    {
        path:'/gods/:id',
        component: require('./pages/God'),
        props: true
    },
    {
        path:'/about',
        component: require('./pages/About')
    },
    {
        path:'/disclaimer',
        component: require('./pages/Disclaimer')
    },
];
