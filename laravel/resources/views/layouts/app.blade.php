<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
@include('frontoffice.partials.head')
<body>
<div class="wrapper application">
    <div class="wrapper-inner" id="app">
        <navigation ref="nav"></navigation>
        <main>
            <header-item ref="header"></header-item>
                <router-view ref="view"></router-view>
            <footer-item ref="footer"></footer-item>
        </main>
    </div>
    @include('frontoffice.partials.scripts')
</div>
</body>
</html>
