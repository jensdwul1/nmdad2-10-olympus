<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
@include('backoffice.partials.head')
<body>
<div class="wrapper backoffice">
        @include('backoffice.partials.sidenav')
                    <div class="wrapper-inner">
        <main>
            @include('backoffice.partials.header')
            <div class="container padded-top" id="app">
                <div class="">
                    @yield('content')
                </div>
            </div>
            @include('backoffice.partials.footer')
            @include('backoffice.partials.scripts')
        </main>
    </div>
</div>
</body>
</html>
