@extends('layouts.backoffice')
@section('content')
        <div class="content">
            <div class="title m-b-md">
                Olympus
            </div>

            <div class="votes">
                <ul>
                @foreach ($votes as $vote)
                    <li class="vote"><a href="/admin/votes/{{$vote->id}}">{{$vote->value}}</a></li>
                @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection