@extends('layouts.backoffice')
@section('title')
    Users | {{$user->name}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content  padded-bottom-xl">
            <div class="card">
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">User Data</h3>
                        <ul>
                            <li><b>Username: </b>{{$user->name}}</li>
                            <li><b>Email: </b>{{$user->email}}</li>
                            <li><b>Password: </b>{{$user->password}}</li>
                        </ul>
                        <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/users/{{$user->id}}/edit/">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </div>
                </div>
            </div>
            @if($user->profile)
            <div class="card vertical">
                <div class="card-image">
                    <img class="profile-image" width="300" src='/img/users/{{$user->profile->profile_picture}}'/>
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$user->profile->firstName.' '.$user->profile->lastName}}</h3>
                        <ul>
                            <li><b>First Name: </b>{{$user->profile->firstName}}</li>
                            <li><b>Last Name: </b>{{$user->profile->lastName}}</li>
                            <li><b>Gender: </b>{{$genders[$user->profile->sex]}}</li>
                            @if($user->profile->dateOfBirth)<li><b>Date of Birth: </b>{{$user->profile->dateOfBirth}}</li>@endif
                            @if($user->profile->phone)<li><b>Phone: </b>{{$user->profile->phone}}</li>@endif
                            @if($user->profile->country)<li><b>Country: </b>{{$user->profile->country}}</li>@endif
                            @if($user->profile->job)<li><b>Job: </b>{{$user->profile->job}}</li>@endif
                            @if($user->profile->description)<li><b>Bio: </b><p>{{$user->profile->description}}</p></li>@endif
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            @if(count($user->elections) > 0)
            <div  class="collection elections">
                @foreach($user->elections as $election)
                    <a href="/admin/elections/{{$election->id}}" class="collection-item election">
                        {{ $election->title }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection