@extends('layouts.backoffice')
@section('title', 'Users | Edit')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block padded-bottom-xl">
            <form method="POST" action="/admin/users/{{$user->id}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}

                <h2>Credentials</h2>
                <div class="input-field col s6">
                    <input id="name" name="name" type="text" class="validate" value="{{ $user->name }}" required>
                    <label for="name">Username</label>
                </div>
                <div class="input-field col s6">
                    <input id="email" name="email" type="email" class="validate" value="{{ $user->email }}" required>
                    <label for="email">Email</label>
                </div>
                <div class="input-field col s6">
                    <input id="password" name="password" type="password" class="validate" value="">
                    <label for="password">Password</label>
                </div>
                <div class="input-field col s6">
                    <input id="password_confirmation" name="password_confirmation" type="password" class="validate" value="{{ $user->password_confirmation }}">
                    <label for="password_confirmation">Password Confirmation</label>
                </div>
                <br>
                <br>
                <h2>Personal Information</h2>
                <div class="input-field col s6">
                    <input id="firstName" name="firstName" type="text" class="validate" value="{{ $user->profile->firstName }}" required>
                    <label for="firstName">First Name</label>
                </div>
                <div class="input-field col s6">
                    <input id="lastName" name="lastName" type="text" class="validate" value="{{ $user->profile->lastName }}">
                    <label for="lastName">Last Name</label>
                </div>
                <div class="file-field input-field">
                    <div class="btn">
                        <span>Profile Picture <i class="material-icons">file_upload</i></span>
                        <input name="picture" id="picture" type="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" name="picture_name" id="picture_name" type="text" value="{{ $user->profile->profile_picture }}">
                    </div>
                </div>
                <div class="input-field col s6">
                    <input id="dateOfBirth" name="dateOfBirth" type="date" class="datepicker" value="{{ $user->profile->dateOfBirth }}">
                    <label for="dateOfBirth">Date of Birth</label>
                </div>
                <div class="input-field col s6">
                    <select name="sex" required>
                        <option value="" disabled selected>Choose a gender</option>
                        @foreach($genders as $key => $gender)
                            <option @if($key == $user->profile->sex) selected @endif  value="{{$key}}">{{ $gender }}</option>
                        @endforeach
                    </select>
                    <label for="sex">Gender</label>
                </div>
                <br>
                <div class="input-field col s6">
                    <input id="phone" name="phone" type="tel" class="validate" value="{{ $user->profile->phone }}">
                    <label for="phone">Phone Number</label>
                </div>
                <div class="input-field col s6">
                    <input id="country" name="country" type="text" class="validate" value="{{ $user->profile->country }}">
                    <label for="country">Country</label>
                </div>
                <div class="input-field col s6">
                    <input id="job" name="job" type="text" class="validate" value="{{ $user->profile->job }}">
                    <label for="job">Job Title</label>
                </div>
                <div class="input-field col s12">
                    <textarea id="description" class="materialize-textarea" name="description">{{ $user->profile->description }}</textarea>
                    <label for="description">Bio</label>
                </div>
                <br>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Edit
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $('select').material_select();
    });
</script>
@endpush