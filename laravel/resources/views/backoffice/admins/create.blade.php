@extends('layouts.backoffice')
@section('title', 'Members | Create')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block padded-bottom-xl">
            <form method="POST" action="/admin/admins" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="input-field col s6">
                    <select name="user" required>
                        <option value="" disabled selected>Choose a User</option>
                        @foreach($users as $key => $user)
                            <option value="{{$user->id}}">{{ $user->profile->firstName.' '.$user->profile->lastName }}</option>
                        @endforeach
                    </select>
                    <label for="user">User</label>
                </div>
                Super Admin:
                <br>
                <div class="input-field col s6">
                    <div class="switch">
                        <label>
                            False
                            <input type="checkbox" name="superadmin">
                            <span class="lever"></span>
                            True
                        </label>
                    </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Create
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $('select').material_select();
    });
</script>
@endpush