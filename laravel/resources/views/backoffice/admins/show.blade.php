@extends('layouts.backoffice')
@section('title')
    Admins | {{$admin->name}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content  padded-bottom-xl">
            <div class="card">
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">Admin Data</h3>
                        <ul>
                            <li><b>Username: </b>{{$admin->user->name}}</li>
                            <li><b>Email: </b>{{$admin->user->email}}</li>
                            @if(Auth::user()->admin && Auth::user()->admin->super_admin)
                                <li><b>Superadmin: </b>{{$admin->super_admin ? "true" : 'false'}}</li>
                            @endif
                        </ul>
                        <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/admins/{{$admin->id}}/edit/">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </div>
                </div>
            </div>
            @if($admin->user->profile)
            <div class="card vertical">
                <div class="card-image">
                    <img class="profile-image" width="300" src='/img/users/{{$admin->user->profile->profile_picture}}'/>
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$admin->user->profile->firstName.' '.$admin->user->profile->lastName}}</h3>
                        <ul>
                            <li><b>First Name: </b>{{$admin->user->profile->firstName}}</li>
                            <li><b>Last Name: </b>{{$admin->user->profile->lastName}}</li>
                            <li><b>Gender: </b>{{$genders[$admin->user->profile->sex]}}</li>
                            @if($admin->user->profile->dateOfBirth)<li><b>Date of Birth: </b>{{$admin->user->profile->dateOfBirth}}</li>@endif
                            @if($admin->user->profile->phone)<li><b>Phone: </b>{{$admin->user->profile->phone}}</li>@endif
                            @if($admin->user->profile->country)<li><b>Country: </b>{{$admin->user->profile->country}}</li>@endif
                            @if($admin->user->profile->job)<li><b>Job: </b>{{$admin->user->profile->job}}</li>@endif
                            @if($admin->user->profile->description)<li><b>Bio: </b><p>{{$admin->user->profile->description}}</p></li>@endif
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            @if(count($admin->elections) > 0)
            <div  class="collection elections">
                @foreach($admin->elections as $election)
                    <a href="/admin/elections/{{$election->id}}" class="collection-item election">
                        {{ $election->title }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection