@extends('layouts.backoffice')
@section('title', 'Gods | Create')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/gods" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="file-field input-field">
                    <div class="btn">
                        <span>File</span>
                        <input name="icon" id="icon" type="file">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" name="icon_name" id="icon_name" type="text">
                    </div>
                </div>
                <div class="input-field col s6">
                    <input id="title" name="title" type="text" class="validate" value="{{ old('title') }}"required>
                    <label for="title">Name</label>
                </div>
                <div class="input-field col s12">
                    <textarea id="body" class="materialize-textarea" name="body">{{ old('body') }}</textarea>
                    <label for="body">Content</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Create
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection