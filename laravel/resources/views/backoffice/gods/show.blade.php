@extends('layouts.backoffice')
@section('title')
    Gods | {{$god->title}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content padded-bottom">
            <div class="card horizontal">
                <div class="card-image">
                    <img class="god-icon " height="200" src='/img/gods/{{$god->icon }}'/>
                    <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/gods/{{$god->id}}/edit/">
                        <i class="material-icons">mode_edit</i>
                    </a>
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$god->title}}</h3>
                        <p>
                            {{$god->body}}
                        </p>
                    </div>
                </div>
            </div>
            @if($god->categories)
            <h3>Categories</h3>
            <div  class="categories collection">
                @foreach($god->categories as $category)
                    <a href="/admin/categories/{{$category->id}}" class="collection-item category">
                        {{ $category->title }}
                    </a>
                @endforeach
            </div>
            @endif
            @if(count($god->elections) > 0)
            <h3>Elections</h3>
            <div  class="collection elections">
                @foreach($god->elections as $election)
                    <a href="/admin/elections/{{$election->id}}" class="collection-item election">
                        {{ $election->title }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection