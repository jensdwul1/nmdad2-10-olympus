@extends('layouts.backoffice')
@section('title', 'Gods | Edit')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/gods/{{$god->id}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="file-field input-field">
                    <div class="btn">
                        <span>File</span>
                        <input name="icon" id="icon" type="file" value="{{$god->icon}}">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" name="icon_name" id="icon_name" type="text" value="{{$god->icon}}">
                    </div>
                </div>
                <div class="input-field col s6">
                    <input id="title" name="title" type="text" class="validate" value="{{$god->title}}"required>
                    <label for="title">Name</label>
                </div>
                <div class="input-field col s12">
                    <textarea id="body" class="materialize-textarea" name="body">{{$god->body}}</textarea>
                    <label for="body">Content</label>
                </div>
                <div class="input-field col s6">
                    <select name="categories[]" multiple>
                        <option value="" disabled selected>Choose one or more categories</option>
                        @foreach($categories as $category)
                            <option @if(in_array($category->id,$categoriesArray)) selected @endif value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                    <label for="categories[]">Categories</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Edit
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $('select').material_select();
    });
</script>
@endpush