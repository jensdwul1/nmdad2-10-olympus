@extends('layouts.backoffice')
@section('title', 'Comments | Create')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/comments" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="input-field col s12">
                    <textarea id="body" class="materialize-textarea" name="body">{{ old('body') }}</textarea>
                    <label for="body">Content</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Create
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection