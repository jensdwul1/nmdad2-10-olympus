@extends('layouts.backoffice')
@section('title')
    Comments | {{$comment->title}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <a href="/admin/users/{{$comment->user->id}}" class="card-title">{{ 'User: '.$comment->user->profile->firstName." ".$comment->user->profile->lastName}}</a>
                        <a href="/admin/referendums/{{$comment->referendum->id}}" class="card-title">{{'Referendum: '.$comment->referendum->id.' - '.$comment->referendum->title}}</a>
                        @if($comment->parent)<a href="/admin/comments/{{$comment->parent->id}}"  class="card-title">{{'Parent Comment ID: '.$comment->parent->id}}</a>@endif
                        <p>
                            {{$comment->body}}
                        </p>
                        <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/comments/{{$comment->id}}/edit">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </div>
                </div>
            </div>
            @if(count($comment->children) > 0)
            <h3>Child Comments</h3>
            <div class="collection children">
                @foreach($comment->children as $child)
                    <a href="/admin/comments/{{$child->id}}" class="collection-item category">
                        <p class="no-margin-top">{{ $child->body }}</p>
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection