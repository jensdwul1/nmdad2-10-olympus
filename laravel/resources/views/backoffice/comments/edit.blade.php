@extends('layouts.backoffice')
@section('title', 'Comments | Edit')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/comments/{{$comment->id}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <input name="referendum_id" type="hidden" value="{{$comment->referendum_id}}">
                <input name="parent_id" type="hidden" value="{{$comment->parent_id}}">
                <input name="user_id" type="hidden" value="{{$comment->user_id}}">
                <div class="input-field col s6">
                    <input id="user" name="user" type="text" value="{{ $comment->user->profile->firstName." ".$comment->user->profile->lastName }}" disabled>
                    <label for="user">User</label>
                </div>
                <div class="input-field col s6">
                    <input id="referendum" name="referendum" type="text" value="{{ $comment->referendum->title }}" disabled>
                    <label for="referendum">Referendum</label>
                </div>
                <div class="input-field col s12">
                    <textarea id="body" class="materialize-textarea" name="body">{{$comment->body}}</textarea>
                    <label for="body">Content</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Edit
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection