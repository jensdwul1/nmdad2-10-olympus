@extends('layouts.backoffice')
@section('title')
    Categorys | {{$category->title}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content padded-bottom">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$category->title}}</h3>
                        <p>
                            {{$category->description}}
                        </p>

                        <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/categories/{{$category->id}}/edit/">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </div>
                </div>
            </div>
            @if($category->gods)
            <h3>Gods</h3>
            <div  class="gods collection">
                @foreach($category->gods as $god)
                    <a href="/admin/gods/{{$god->id}}" class="collection-item category">
                        {{ $god->title }}
                    </a>
                @endforeach
            </div>
            @endif
            @if(count($category->referendums) > 0)
            <h3>Referendums</h3>
            <div  class="collection referendums">
                @foreach($category->referendums as $referendum)
                    <a href="/admin/referendums/{{$referendum->id}}" class="collection-item referendum">
                        {{ $referendum->title }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection