@extends('layouts.backoffice')
@section('title')
    Pantheons | {{$pantheon->startDate}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="card">
                <div class="card-stacked">
                    <div class="card-content">
                        <blockquote><b>Start Date:</b> {{$pantheon->startDate}}</blockquote>
                        <blockquote><b>End Date:</b> {{$pantheon->endDate}}</blockquote>
                    </div>
                    <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/pantheons/{{$pantheon->id}}/edit">
                        <i class="material-icons">mode_edit</i>
                    </a>
                </div>
            </div>
            @if(count($pantheon->members) > 0)
            <div  class="collection members">
                @foreach($pantheon->members as $member)
                    <a href="/admin/members/{{$member->id}}" class="collection-item member">
                        {{ $member->god->title.': '.$member->user->profile->firstName.' '.$member->user->profile->lastName }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection