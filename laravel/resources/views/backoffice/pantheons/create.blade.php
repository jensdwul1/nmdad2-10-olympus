@extends('layouts.backoffice')
@section('title', 'Pantheon | Create')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/pantheons" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="input-field col s6">
                    <input id="startDate" name="startDate" type="date" class="datepicker" value="{{ old('startDate') }}">
                    <label for="startDate">Start Date</label>
                </div>
                <div class="input-field col s6">
                    <input id="endDate" name="endDate" type="date" class="datepicker" value="{{ old('endDate') }}">
                    <label for="endDate">End Date</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Create
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $('select').material_select();
    });
</script>
@endpush