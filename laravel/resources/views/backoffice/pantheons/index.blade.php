@extends('layouts.backoffice')
@section('title', 'Pantheons | Overview')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content flex column">
            <a class="btn-floating waves-effect waves-light grey darken-2 flex-child end" href="/admin/pantheons/create">
                <i class="material-icons">add</i>
            </a>
            <table class="responsive-table">
                <thead>
                    <tr>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Created</th>
                        <th>Updated</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($pantheons as $pantheon)
                    <tr class="table-row" data-id="{{$pantheon->id}}">
                        <td title="{{$pantheon->startDate}}"><a href="/admin/pantheons/{{$pantheon->id}}" title="Edit">{{$pantheon->startDate}}</a></td>
                        <td title="{{$pantheon->endDate}}">{{$pantheon->endDate}}</td>
                        <td title="{{$pantheon->created_at}}">{{$pantheon->created_at->diffForHumans()}}</td>
                        <td title="{{$pantheon->updated_at}}">{{$pantheon->updated_at->diffForHumans()}}</td>
                        <td>
                            <a class="btn-flat padded-left-s padded-right-s" href="/admin/pantheons/{{$pantheon->id}}" title="Show">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a class="btn-flat padded-left-s padded-right-s" href="/admin/pantheons/{{$pantheon->id}}/edit" title="Edit">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form name="delete" action="{{ '/admin/pantheons/delete/'.$pantheon->id}}" method="POST" style="display: inline-block;">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn-delete @if($pantheon->trashed()) deleted @endif btn-flat padded-left-s padded-right-s" title="Soft Delete" data-title="{{$pantheon->title}}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            <form name="crush" action="{{ '/admin/pantheons/crush/'.$pantheon->id}}" method="POST" style="display: inline-block;">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn-delete btn-crush btn-flat padded-left-s padded-right-s" title="Hard Delete" data-title="This Pantheon">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div id="modal-delete" class="modal">
                <div class="modal-content">
                    <h4>Hard Deleting - Pantheon Name</h4>
                    <p>Are you sure you wish to permanently remove this pantheon?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Disagree</a>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat confirm" data-id="0" onclick="confirmHardDelete()">Agree</a>
                </div>
            </div>
            {{ $pantheons->links() }}
        </div>
    </div>
@endsection
@push('scripts-bottom')
<script>
    $(function () {
        // Delete buttons
        $('button.btn-crush').on('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            var self = $(this),
                row = self.parents('.table-row');
            $('#modal-delete h4').text('Hard Deleting -'+self.data('title'));
            document.querySelector('#modal-delete .modal-action.confirm').dataset.id = row.data('id');
            $('#modal-delete').modal('open');
        });
        console.log('Does this even work?');
        $('#modal-delete').modal({
                dismissible: false, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                inDuration: 300, // Transition in duration
                outDuration: 200, // Transition out duration
                startingTop: '4%', // Starting top style attribute
                endingTop: '10%', // Ending top style attribute
                ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    console.log(modal, trigger);
                },
                complete: function(e) {
                    //console.log(e,'event');
                } // Callback for Modal close
            }
        );
    });

    function confirmHardDelete(){
        var objectId = document.querySelector('#modal-delete .modal-action.confirm').dataset.id;
        console.log('Row ID',objectId);
        var record = $('.table-row[data-id='+objectId+']');
        var recordForm = record.find('form').submit();
        console.log('Record for delete',record);
        record.hide(400);
    }
</script>
@endpush