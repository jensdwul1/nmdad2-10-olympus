@extends('layouts.backoffice')
@section('title')
    Referendums | {{$referendum->title}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="card">
                <div class="card-stacked">
                    <div class="card-content">
                        <h4 class="">{{$referendum->title}}</h4>
                        <h6>{{$referendum->user->profile->firstName.' '.$referendum->user->profile->lastName}}</h6>
                        <p>{{$referendum->description}}</p>
                        <blockquote><b>Start Date:</b> {{$referendum->startDate}}</blockquote>
                        <blockquote><b>End Date:</b> {{$referendum->endDate}}</blockquote>
                    </div>
                    <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/referendums/{{$referendum->id}}/edit">
                        <i class="material-icons">mode_edit</i>
                    </a>
                </div>
            </div>

            @if(count($referendum->categories) > 0)
                <h3>Categories</h3>
                <div  class="categories collection">
                    @foreach($referendum->categories as $category)
                        <a href="/admin/categories/{{$category->id}}" class="collection-item category">
                            {{ $category->title }}
                        </a>
                    @endforeach
                </div>
            @endif
            @if(count($referendum->comments) > 0)
                <h3>Comments</h3>
                <div  class="collection comments">
                    @foreach($referendum->comments as $comment)
                        @if($comment->parent_id == null)
                            <a href="/admin/comments/{{$comment->id}}" class="collection-item comment">
                                <p class="comment-body no-margin-top">{{ $comment->body }}</p>
                                <span class="comment-date">{{ $comment->user->name.' - '.$comment->created_at->diffForHumans() }}</span>
                            </a>
                            @foreach($comment->children as $child)
                                <a href="/admin/comments/{{$child->id}}" class="collection-item comment child">
                                    <p class="comment-body no-margin-top">{{ $child->body }}</p>
                                    <span class="comment-date">{{  $child->user->name.' - '.$child->created_at->diffForHumans() }}</span>
                                </a>
                            @endforeach
                        @endif
                    @endforeach
                </div>
            @endif
            <style>
                .content {
                    padding-bottom: 40px;
                }
                .comment.child {
                    padding-left: 30px;
                    position: relative;
                }
                .comment.child::after {
                    width:10px;
                    content:'';
                    background: #fbae17;
                    height: 100%;
                    position: absolute;
                    left:0;
                    top:0;
                }
            </style>
        </div>
    </div>
@endsection