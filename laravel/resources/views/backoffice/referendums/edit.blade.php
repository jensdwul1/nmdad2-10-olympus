@extends('layouts.backoffice')
@section('title', 'Referendums | Edit')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/referendums/{{$referendum->id}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="input-field col s6">
                    <input id="title" name="title" type="text" class="validate" value="{{ $referendum->title }}"required>
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s6">
                    <input id="startDate" name="startDate" type="date" class="datepicker" value="{{ $referendum->startDate }}">
                    <label for="startDate">Start Date</label>
                </div>
                <div class="input-field col s6">
                    <input id="endDate" name="endDate" type="date" class="datepicker" value="{{ $referendum->endDate }}">
                    <label for="endDate">End Date</label>
                </div>
                <div class="input-field col s12">
                    <textarea id="description" class="materialize-textarea" name="description">{{$referendum->description}}</textarea>
                    <label for="description">Content</label>
                </div>
                <div class="input-field col s6">
                    <select name="categories[]" multiple>
                        <option value="" disabled selected>Choose one or more categories</option>
                        @foreach($categories as $category)
                            <option @if(in_array($category->id,$categoriesArray)) selected @endif value="{{$category->id}}">{{$category->title}}</option>
                        @endforeach
                    </select>
                    <label for="categories[]">Categories</label>
                </div>
                <div class="input-field col s6">
                    <select name="user">
                        <option value="" disabled selected>Choose a user</option>
                        @foreach($profiles as $profile)
                            <option @if($profile->user->id == $referendum->user->id) selected @endif value="{{$profile->user->id}}">{{$profile->firstName.' '.$profile->lastName}}</option>
                        @endforeach
                    </select>
                    <label for="user">User</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Edit
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $('select').material_select();
    });
</script>
@endpush