@extends('layouts.backoffice')
@section('title', 'Achievements | Edit')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/achievements/{{$achievement->id}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="file-field input-field">
                    <div class="btn">
                        <span>File</span>
                        <input name="icon" id="icon" type="file" value="{{$achievement->icon}}">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" name="icon_name" id="icon_name" type="text" value="{{$achievement->icon}}">
                    </div>
                </div>
                <div class="input-field col s6">
                    <input id="title" name="title" type="text" class="validate" value="{{$achievement->title}}"required>
                    <label for="title">Name</label>
                </div>
                <div class="input-field col s12">
                    <textarea id="description" class="materialize-textarea" name="description">{{$achievement->description}}</textarea>
                    <label for="description">Content</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Edit
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection