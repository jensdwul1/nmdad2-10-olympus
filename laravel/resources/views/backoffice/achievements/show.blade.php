@extends('layouts.backoffice')
@section('title')
    Achievements | {{$achievement->title}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="card horizontal">
                <div class="card-image">
                    <img class="achievement-icon " height="200" src='/img/achievements/{{$achievement->icon }}'/>
                    <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/achievements/{{$achievement->id}}/edit/">
                        <i class="material-icons">mode_edit</i>
                    </a>
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$achievement->title}}</h3>
                        <p>
                            {{$achievement->description}}
                        </p>
                    </div>
                </div>
            </div>
            @if($achievement->users)
            <div  class="categories collection">
                @foreach($achievement->users as $user)
                    <a href="/admin/users/{{$user->id}}" class="collection-item user">
                        {{ $user->profile->firstName.' '.$user->profile->lastName }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection