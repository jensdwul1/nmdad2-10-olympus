<div class="side-nav fixed" id="nav-mobile">
    <ul>
        <li class="logo">
            <a id="logo-container" href="/admin/" class="brand-logo">
                <object id="front-page-logo" type="image/svg+xml" data="/img/logo/logo.svg">Your browser does not support SVG</object>
            </a>
        </li>
        <hr>

        @if (Auth::guest())
        <li>
            <p class="padded-left padded-right text-center">
                @php
                    $hour = date('G');
                    if($hour >= 5 && $hour <= 11){
                        echo 'Good Morning User';
                    } elseif($hour >= 12 && $hour <= 18){
                        echo 'Good Afternoon User';
                    } elseif($hour >= 19 && $hour <= 22){
                        echo 'Good Evening User';
                    } elseif($hour >= 23 || $hour <= 4){
                        echo 'Good Evening User';
                    } else {
                        echo 'Good Day User';
                    }
                @endphp
            </p>
        </li>
        @else
        <li>
            <p class="padded-left padded-right text-center">
                @php
                    $hour = date('G');
                    if($hour >= 5 && $hour <= 11){
                        echo 'Good Morning '.Auth::user()->name;
                    } elseif($hour >= 12 && $hour <= 18){
                        echo 'Good Afternoon '.Auth::user()->name;
                    } elseif($hour >= 19 && $hour <= 22){
                        echo 'Good Evening '.Auth::user()->name;
                    } elseif($hour >= 23 || $hour <= 4){
                        echo 'Good Evening '.Auth::user()->name;
                    } else {
                        echo 'Good Day '.Auth::user()->name;
                    }
                @endphp
            </p>
        </li>
        @endif
        <hr>
        @if(Auth::check())
        <li>
            <a class="item" href="/admin/">
                <i class="material-icons left">home</i>
                <label>Dashboard</label>
            </a>
        </li>
        <hr>
        <li>
            <a class="item" href="/admin/achievements">
                <i class="material-icons left">local_activity</i>
                <label>Achievements</label>
            </a>
        </li>
        <li>
            <a class="item" href="/admin/gods">
                <i class="material-icons left">settings_input_antenna</i>
                <label>Gods</label>
            </a>
        </li>
        <li>
            <a class="item" href="/admin/pantheons">
                <i class="material-icons left">business</i>
                <label>Pantheon</label>
            </a>
        </li>
        <li>
            <a class="item" href="/admin/elections">
                <i class="material-icons left">track_changes</i>
                <label>Elections</label>
            </a>
        </li>
        <li>
            <a class="item" href="/admin/referendums">
                <i class="material-icons left">forum</i>
                <label>Referendums</label>
            </a>
        </li>
        <hr>
        <li>
            <a class="item" href="/admin/users">
                <i class="material-icons left">recent_actors</i>
                <label>Users</label>
            </a>
        </li>
        @if (Auth::guest())
            <li>
                <a href="{{ route('login') }}">
                    <i class="material-icons left">power_settings_new</i>
                    <label>Login</label>
                </a>
            </li>
        @else
            @if(Auth::user()->admin && Auth::user()->admin->super_admin)
                <li>
                    <a class="item" href="/admin/admins">
                        <i class="material-icons left">assignment_ind</i>
                        <label>Admins</label>
                    </a>
                </li>
            @endif
            <li>
                <a class="item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="material-icons left">exit_to_app</i>
                    <label>Log off</label>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        @endif
        @endif
    </ul>
</div>