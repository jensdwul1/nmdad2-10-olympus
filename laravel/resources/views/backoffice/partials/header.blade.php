<nav class="header-content">
    <div class="nav-wrapper flex">
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        <h2 class="padded-left no-margin flex-child stretch">@yield('title')</h2>
    </div>
</nav>