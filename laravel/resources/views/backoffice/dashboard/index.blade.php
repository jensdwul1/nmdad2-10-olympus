@extends('layouts.backoffice')
@section('title', 'Dashboard')
@section('content')
    <div class="dashboard flex wrap align-stretch">
        <a href="/admin/pantheons/{{$cur_pantheon->id}}" class="dashboard-block text-center flex column centered justified-c flex-child stretch" title="{{'Pantheon of '.date('F')}}">
            <div class="dashboard-block-content">
                <h3><i class="material-icons left">business</i>{{date('F')}}</h3>
            </div>
        </a>
        <a href="/admin/referendums/actives" class="dashboard-block text-center flex column centered justified-c flex-child stretch" title="Active Referendums">
            <div class="dashboard-block-content">
                <h3><i class="material-icons left">forum</i>{{$cur_referendums}}</h3>
            </div>
        </a>
        <a href="/admin/users/" class="dashboard-block text-center flex column centered justified-c flex-child stretch" title="New Users">
            <div class="dashboard-block-content">
                <h3><i class="material-icons left">recent_actors</i>{{$users}}</h3>
            </div>
        </a>

    </div>

@endsection