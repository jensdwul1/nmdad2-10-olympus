@extends('layouts.backoffice')
@section('title', 'Members | Create')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/members" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="input-field col s6">
                    <select name="user">
                        <option value="" disabled selected>Choose a user</option>
                        @foreach($profiles as $profile)
                            <option value="{{$profile->user->id}}">{{$profile->firstName.' '.$profile->lastName}}</option>
                        @endforeach
                    </select>
                    <label for="user">User</label>
                </div>
                <div class="input-field col s6">
                    <select name="pantheon">
                        <option value="" disabled selected>Choose a pantheon</option>
                        @foreach($pantheons as $pantheon)
                            <option value="{{$pantheon->id}}">{{$pantheon->startDate}}</option>
                        @endforeach
                    </select>
                    <label for="pantheon">Pantheon</label>
                </div>
                <div class="input-field col s6">
                    <select name="god">
                        <option value="" disabled selected>Choose a god</option>
                        @foreach($gods as $god)
                            <option value="{{$god->id}}">{{$god->title}}</option>
                        @endforeach
                    </select>
                    <label for="god">God</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Create
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('select').material_select();
    });
</script>
@endpush