@extends('layouts.backoffice')
@section('title')
    Members | {{$member->user->profile->firstName.' '.$member->user->profile->lastName}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$member->user->profile->firstName.' '.$member->user->profile->lastName}}</h3>
                        <blockquote>{{$member->pantheon->startDate}}</blockquote>
                        <blockquote>{{$member->god->title}}</blockquote>
                        <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/members/{{$member->id}}/edit/">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </div>
                </div>
            </div>
            {{--@if($member->categories)
            <div  class="categories">
                @foreach($member->categories as $category)
                    <a href="/admin/categories/{{$category->id}}" class="collection-item category">
                        {{ $category->title }}
                    </a>
                @endforeach
            </div>
            @endif --}}
            @if(count($member->elections) > 0)
            <div  class="collection elections">
                @foreach($member->elections as $election)
                    <a href="/admin/elections/{{$election->id}}" class="collection-item election">
                        {{ $election->title }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection