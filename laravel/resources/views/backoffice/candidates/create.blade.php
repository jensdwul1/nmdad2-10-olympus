@extends('layouts.backoffice')
@section('title', 'Candidates | Create')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/candidates" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="input-field col s6">
                    <select name="user">
                        <option value="" disabled selected>Choose a user</option>
                        @foreach($profiles as $profile)
                            <option value="{{$profile->user->id}}">{{$profile->firstName.' '.$profile->lastName}}</option>
                        @endforeach
                    </select>
                    <label for="user">User</label>
                </div>
                <div class="input-field col s6">
                    <select name="election">
                        <option value="" disabled selected>Choose a election</option>
                        @foreach($elections as $election)
                            <option value="{{$election->id}}">{{$election->title}}</option>
                        @endforeach
                    </select>
                    <label for="election">Election</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Create
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('select').material_select();
    });
</script>
@endpush