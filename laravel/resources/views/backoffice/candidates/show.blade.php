@extends('layouts.backoffice')
@section('title')
    Candidates | {{$candidate->user->profile->firstName.' '.$candidate->user->profile->lastName}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="card horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$candidate->user->profile->firstName.' '.$candidate->user->profile->lastName}}</h3>
                        <blockquote>{{$candidate->election->title}}</blockquote>
                        <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/candidates/{{$candidate->id}}/edit/">
                            <i class="material-icons">mode_edit</i>
                        </a>
                    </div>
                </div>
            </div>
            @if(count($candidate->elections) > 0)
            <div  class="collection elections">
                @foreach($candidate->elections as $election)
                    <a href="/admin/elections/{{$election->id}}" class="collection-item election">
                        {{ $election->title }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection