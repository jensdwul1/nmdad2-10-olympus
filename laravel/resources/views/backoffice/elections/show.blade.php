@extends('layouts.backoffice')
@section('title')
    Elections | {{$election->title}}
@endsection
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="card horizontal">
                <div class="card-image">
                    <img class="election-icon " height="200" src='/img/gods/{{$election->god->icon }}'/>
                    <a class="btn-floating halfway-fab waves-effect waves-light grey darken-2 flex-child end" href="/admin/elections/{{$election->id}}/edit">
                        <i class="material-icons">mode_edit</i>
                    </a>
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h3 class="card-title">{{$election->title}}</h3>
                        <h4>{{$election->god->title}}</h4>
                        <blockquote><b>Start Date:</b> {{$election->startDate}}</blockquote>
                        <blockquote><b>End Date:</b> {{$election->endDate}}</blockquote>
                    </div>
                </div>
            </div>
            @if(count($election->candidates) > 0)
            <div  class="collection candidates">
                @foreach($election->candidates as $candidate)
                    <a href="/admin/candidates/{{$candidate->id}}" class="collection-item candidate">
                        {{ $candidate->user->profile->firstName.' '.$candidate->user->profile->lastName }}
                    </a>
                @endforeach
            </div>
            @endif
        </div>
    </div>
@endsection