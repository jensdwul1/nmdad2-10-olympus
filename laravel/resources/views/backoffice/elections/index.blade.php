@extends('layouts.backoffice')
@section('title', 'Elections | Overview')
@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content flex column">
            <a class="btn-floating waves-effect waves-light grey darken-2 flex-child end" href="/admin/elections/create">
                <i class="material-icons">add</i>
            </a>
            <table class="responsive-table">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Updated</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($elections as $election)
                    <tr class="table-row" data-id="{{$election->id}}">
                        <td><a href="/admin/elections/{{$election->id}}" title="Edit {{$election->title}}">{{$election->title}}</a></td>
                        <td title="{{$election->startDate}}">{{$election->startDate}}</td>
                        <td title="{{$election->endDate}}">{{$election->endDate}}</td>
                        <td title="{{$election->updated_at}}">{{$election->updated_at->diffForHumans()}}</td>
                        <td>
                            <a class="btn-flat padded-left-s padded-right-s" href="/admin/elections/{{$election->id}}" title="Show {{$election->title}}">
                                <i class="fa fa-eye"></i>
                            </a>
                            <a class="btn-flat padded-left-s padded-right-s" href="/admin/elections/{{$election->id}}/edit/" title="Edit {{$election->title}}">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <form name="delete" action="{{ '/admin/elections/delete/'.$election->id}}" method="POST" style="display: inline-block;">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn-delete @if($election->trashed()) deleted @endif btn-flat padded-left-s padded-right-s" title="Soft Delete {{$election->title}}" data-title="{{$election->title}}">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            <form name="crush" action="{{ '/admin/elections/crush/'.$election->id}}" method="POST" style="display: inline-block;">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button class="btn-delete btn-crush btn-flat padded-left-s padded-right-s" title="Hard Delete {{$election->title}}" data-title="{{$election->title}}">
                                    <i class="fa fa-remove"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <div id="modal-delete" class="modal">
                <div class="modal-content">
                    <h4>Hard Deleting - God Name</h4>
                    <p>Are you sure you wish to permanently remove this god?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Disagree</a>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat confirm" data-id="0" onclick="confirmHardDelete()">Agree</a>
                </div>
            </div>
            <div id="modal-delete" class="modal">
                <div class="modal-content">
                    <h4>Hard Deleting - Election Name</h4>
                    <p>Are you sure you wish to permanently remove this election?</p>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Disagree</a>
                    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat confirm" data-id="0" onclick="confirmHardDelete()">Agree</a>
                </div>
            </div>
            {{ $elections->links() }}
        </div>
    </div>
@endsection
@push('scripts-bottom')
<script>
    $(function () {
        // Delete buttons
        $('button.btn-crush').on('click', function (e) {
            e.stopPropagation();
            e.preventDefault();
            var self = $(this),
                row = self.parents('.table-row');
            $('#modal-delete h4').text('Hard Deleting -'+self.data('title'));
            document.querySelector('#modal-delete .modal-action.confirm').dataset.id = row.data('id');
            $('#modal-delete').modal('open');
        });
        $('#modal-delete').modal({
                dismissible: false, // Modal can be dismissed by clicking outside of the modal
                opacity: .5, // Opacity of modal background
                inDuration: 300, // Transition in duration
                outDuration: 200, // Transition out duration
                startingTop: '4%', // Starting top style attribute
                endingTop: '10%', // Ending top style attribute
                ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                    console.log(modal, trigger);
                },
                complete: function(e) {
                    //console.log(e,'event');
                } // Callback for Modal close
            }
        );
    });

    function confirmHardDelete(){
        var objectId = document.querySelector('#modal-delete .modal-action.confirm').dataset.id;
        console.log('Row ID',objectId);
        var record = $('.table-row[data-id='+objectId+']');
        var recordForm = record.find('form').submit();
        console.log('Record for delete',record);
        record.hide(400);
    }
</script>
@endpush