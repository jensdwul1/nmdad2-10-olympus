@extends('layouts.backoffice')
@section('title', 'Elections | Create')
@section('content')
    <div class="flex-center position-ref full-height padded-top">
        <div class="content form-block">
            <form method="POST" action="/admin/elections" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="input-field col s6">
                    <input id="title" name="title" type="text" class="validate" value="{{ old('title') }}"required>
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s6">
                    <input id="startDate" name="startDate" type="date" class="datepicker" value="{{ old('startDate') }}">
                    <label for="startDate">Start Date</label>
                </div>
                <div class="input-field col s6">
                    <input id="endDate" name="endDate" type="date" class="datepicker" value="{{ old('endDate') }}">
                    <label for="endDate">End Date</label>
                </div>
                <div class="input-field col s6">
                    <select name="god">
                        <option value="" disabled selected>Choose a god</option>
                        @foreach($gods as $god)
                            <option value="{{$god->id}}">{{$god->title}}</option>
                        @endforeach
                    </select>
                    <label for="god">God</label>
                </div>
                <div class="form-group">
                    <button class="btn waves-effect waves-light" type="submit">
                        <i class="material-icons right">send</i>
                        Create
                    </button>
                </div>
                @include('backoffice.partials.errors')
            </form>
        </div>
    </div>
@endsection

@push('scripts-bottom')
<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('.datepicker').pickadate({
            format: 'yyyy-mm-dd',
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 15 // Creates a dropdown of 15 years to control year
        });
        $('select').material_select();
    });
</script>
@endpush