<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Olympus</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!--<link href="/css/vendor/foundation.css" rel="stylesheet" type="text/css">-->
    <link href="/css/app.css" rel="stylesheet" type="text/css">

    <!-- Styles -->

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- CSRF Token accepted -->
    <script>
        window.Laravel =  {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
