<div class="header-content">
    <div class="nav-wrapper flex">
        <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
        <a id="logo-container" href="/" class="brand-logo flex flex-child justified-c stretch">
            <object id="front-page-logo" type="image/svg+xml" data="/img/logo/logo-transparent.svg">Your browser does not support SVG</object>
        </a>
    </div>
</div>